# ShEngine 2.0 #
---

ShEngine2 is a 2D game engine dedicated at easily creating the game logic with pre-implemented basic engine features (physics, rendering, assets...)

## Features ##
---

### Rendering ###

* SFML
* Textures Dynamic/Static handling
* Fonts Dynamic/Static handling

### Core ###

* Data driven configuration
* GD Specific Handlers
* Broadcast/Messaging system
* GC Base

### I/O ###

* XML I/O Config/DD
* Abstract I/O Containers
* SFML I/O

### State Machine/Levels ###

* Abstract FSM
* DD Stages controller

### Entities ###

* DD-ECS
* M.Abstract Ent. Factory
* System Components
* Nested DD-Parenting ECS

### AI ###

Work in progress

### Physics ###

Work in progress

### System ###

* SFML Clock
* Gameclock

### Debug ###

Work in progress

### UI ###

* Qt Driven UI
* Qt Shengine2 Lib Impl.

### Networking ###

Work in progress

## Platforms ##
---

* PC (Multiple controllers)

## Usage ##
---

Win32/Win64 Versions (Crossplatform is WIP)

* Load the project with Visual Studio
* Build the project with engine and libs
* Load the project examples .pro with Qt
* Build the project with Qt, engine and libs
* Include all DLLs from Dev/Src/DLL in Dev/Bin/JRExe/(Release/Debug)/

## Development Quality ##
---

Documentation: Very low yet - WIP, feel free to make one  
Code quality: Good - Prototype engine, ready for optimization

## Dependencies and Libs ##
---

* Included (Rt+Inc): SFML Library - [http://www.sfml-dev.org/]()
* Included (Rt+Inc): TinyXML - [http://www.grinninglizard.com/tinyxml2/]()
* Included (Rt): Qt - [http://www.qt.io/]()

## Licence ##
---

This project is under MIT Licence.  
  
The MIT License (MIT)  
  
Copyright (c) 2015 Jeremy Dubuc  
  
Permission is hereby granted, free of charge, to any person obtaining a copy  
of this software and associated documentation files (the "Software"), to deal  
in the Software without restriction, including without limitation the rights  
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell  
copies of the Software, and to permit persons to whom the Software is  
furnished to do so, subject to the following conditions:  
  
The above copyright notice and this permission notice shall be included in  
all copies or substantial portions of the Software.  
  
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN  
THE SOFTWARE.
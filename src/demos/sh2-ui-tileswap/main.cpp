
#include <QApplication>
#include "mainwindow.h"
#include "shengine2/shengine2.h"
#include "core.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    Core core;

    core.load();
    core.run();

    w.show();
    return a.exec();
}

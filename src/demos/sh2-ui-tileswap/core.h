#ifndef CORE_H
#define CORE_H

#include "shengine2/shengine2.h"

struct GameData
{
    GameData()
    {
        moveDir = 0;
    }

    int moveDir;
};

class Core : public jre2::IGameContainer
{
private:
    jre2::RenderingEngine		m_render;
    jre2::AssetsEngine			m_assets;
    jre2::FSM					m_fsm;
    jre2::GameClock				m_time;
    jre2::StageEngine			m_stages;
    jre2::EventsEngine			m_dispatch;

    jre2::InputHandler			m_input;
    //GameInputsEvents			m_inputEventsHnd;
    jre2::GUIController			m_gui;

    GameData					m_gameData;
    jre2::Configuration			m_config;

    jre2::Camera2D				m_mainCam;
    jre2::GameObjectFactory		m_goFactory;
    jre2::ComponentsFactory		m_compFactory;

    jre2::DebugController		m_debug;
public:
    Core();
    virtual ~Core();

    void load();
    EXECRES run();
    void exit();

    virtual jre2::AssetsEngine* getAssetsEngine();
    virtual jre2::FSM* getFSM();
    virtual jre2::RenderingEngine* getRenderingEngine();
    virtual jre2::GameClock* getGameClock();
    virtual jre2::StageEngine* getStageEngine();
    virtual jre2::InputHandler* getInputHandler();
    virtual GameData* getGameData();
    virtual jre2::Configuration* getConfiguration();
    virtual jre2::EventsEngine* getEventsEngine();
    virtual jre2::GUIController* getGUIController();
    virtual jre2::GameObjectFactory* getGameObjectFactory();
    virtual jre2::ComponentsFactory* getComponentsFactory();

    // Debug engines
    virtual jre2::DebugController* getDebugController();
};

#endif // CORE_H

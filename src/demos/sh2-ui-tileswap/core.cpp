/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include <shengine2/shengine2.h>
#include "core.h"

Core::Core()
    : m_config("Data/config.xml")
{

}

Core::~Core()
{

}

void Core::load()
{
    // Load engines
    m_config.load();
    //m_assets.setHandler(new StaticHandler());
    m_assets.init(&m_render);
    m_assets.load();

    // Feed factories
    m_goFactory.addBlueprint(new jre2::Sprite());
    m_goFactory.addBlueprint(new jre2::Text());
    m_compFactory.addBlueprint(new jre2::Transform());
    m_compFactory.addBlueprint(new jre2::DataBindings());
    m_compFactory.addBlueprint(new jre2::RenderShape());
    m_compFactory.addBlueprint(new jre2::RenderLabel());

    //m_inputEventsHnd.init(this);
    m_input.add(new jre2::SFMLInput());
    //m_input.init(&m_inputEventsHnd);
    m_render.getRenderWindow(GAMEWINDOW).init("game", this);
    m_render.getRenderWindow(DEBUGWINDOW).init("debug", this);
    //m_fsm.init(new GameImpl());

    // Load stages and dynamic infos
    m_stages.init();
    m_stages.addStage(new jre2::XMLStage(this, "Data/Stages/01.xml"));

    // Start engines
    m_mainCam.setupFullsize(&m_render.getRenderWindow());
    m_mainCam.move(sf::Vector2f(50, -200));
    m_render.getRenderWindow(GAMEWINDOW).setCamera(CONTEXT_MAIN, &m_mainCam);
    m_render.getRenderWindow(GAMEWINDOW).setCamera(CONTEXT_UI, NULL);
    m_render.getRenderWindow(GAMEWINDOW).setActiveContext(CONTEXT_MAIN);
    m_render.getRenderWindow(GAMEWINDOW).create();
    m_render.getRenderWindow(DEBUGWINDOW).setCamera(CONTEXT_MAIN, NULL);
    m_render.getRenderWindow(DEBUGWINDOW).setCamera(CONTEXT_UI, NULL);
    m_render.getRenderWindow(DEBUGWINDOW).setActiveContext(CONTEXT_UI);
    m_render.getRenderWindow(DEBUGWINDOW).create();

    // Init debugging engines and links
    m_debug.init(this);
}

EXECRES Core::run()
{
    static float m_timer = 0.0;
    static int fps = 0;
    static float sleepDelay = 0.0;
    float dt = m_time.getElapsedTime().asSeconds();

    fps++;
    if (dt > m_timer)
    {
        m_timer++;
        std::cout << "FPS (u+r): " << fps << std::endl;

        //if (fps >= 30) {
        //	sleepDelay = 1000 / fps;
        //	std::cout << "Sleepd " << sleepDelay << std::endl;
        //}
        //else {
        //	sleepDelay = 0;
        //}
        fps = 0;
    }
    //if (sleepDelay > 0)
    //{
    //	Sleep(sleepDelay);
    //}

    m_fsm.update(this);
    if (m_fsm.getState() == STATE_EXIT)
        return (E_EXITOK);
    return (E_CONTINUE);
}

void Core::exit()
{
    m_assets.unload();
    m_render.getRenderWindow().destroy();
}

jre2::AssetsEngine* Core::getAssetsEngine() { return (&m_assets); }
jre2::FSM* Core::getFSM() { return (&m_fsm); }
jre2::RenderingEngine* Core::getRenderingEngine() { return (&m_render); }
jre2::GameClock* Core::getGameClock() { return (&m_time); }
jre2::StageEngine* Core::getStageEngine() { return (&m_stages); }
jre2::InputHandler* Core::getInputHandler() { return (&m_input); }
GameData* Core::getGameData() { return (&m_gameData); }
jre2::Configuration* Core::getConfiguration() { return (&m_config); }
jre2::EventsEngine* Core::getEventsEngine() { return (&m_dispatch); }
jre2::GUIController* Core::getGUIController() { return (&m_gui); }
jre2::GameObjectFactory* Core::getGameObjectFactory() { return (&m_goFactory); }
jre2::DebugController* Core::getDebugController() { return (&m_debug); }
jre2::ComponentsFactory* Core::getComponentsFactory() { return (&m_compFactory); }






#-------------------------------------------------
#
# ShEngine2 Project Generated File
#
#-------------------------------------------------

#-------------------------------------------------
# Proj settings
#-------------------------------------------------
ROOT="../../.."
ARCH="x64"
#win32:ARCH="Win32"

#-------------------------------------------------
# Lib settings
#-------------------------------------------------
SHE2_LIBPATH        = "$$ROOT/../lib/$$ARCH"
SHE2_INCPATH        = "$$ROOT/includes"
SHE2_INCPATH_SHE2   = "$$ROOT/includes/shengine2"
SHE2_LIB            = "ShEngine2-$$ARCH"
SHE2_LIB_D          = "ShEngine2-$$ARCH-d"
LIBSUFFIX           = ""

#-------------------------------------------------
# Release dir settings
#-------------------------------------------------
#CONFIG(release) {
#    TARGET      = sh2-ui-tileswap
#    LIBS        += "-L$$SHE2_LIBPATH -l$$SHE2_LIB"
#}
#Release:DESTDIR = "$$ROOT/../bin/$$ARCH/demos/$$TARGET/"

#-------------------------------------------------
# Debug dir settings
#-------------------------------------------------
CONFIG(debug) {
    TARGET      = sh2-ui-tileswap-d
#    LIBS        += "-L$$SHE2_LIBPATH -l$$SHE2_LIB_D"
    LIBSUFFIX   = "-d"
}
Debug:DESTDIR = "$$ROOT/../bin/$$ARCH/demos/$$TARGET/"

#-------------------------------------------------
# Build settings
#-------------------------------------------------

#CONFIG(debug) {
#    LIBS +=     "$$SHE2_LIBPATH/$$SHE2_LIB_D.lib"
#} else {
#    LIBS +=     "$$SHE2_LIBPATH/$$SHE2_LIB.lib"
#}
LIBS +=     "$$SHE2_LIBPATH/ShEngine2-x64-d.lib"
LIBS +=     "$$SHE2_LIBPATH/ShEngine2-x64-d.lib"
LIBS +=     "$$SHE2_LIBPATH/tinyxml2-d.lib"
LIBS +=     "$$SHE2_LIBPATH/SFML/sfml-graphics-d.lib"
LIBS +=     "$$SHE2_LIBPATH/SFML/sfml-window-d.lib"
LIBS +=     "$$SHE2_LIBPATH/SFML/sfml-system-d.lib"

#fix this
DEFINES     += _WIN32
DEFINES     += _SFML
DEFINES     += _USE_STL

INCLUDEPATH += "$$SHE2_INCPATH"

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app


SOURCES +=  main.cpp\
            mainwindow.cpp \
            core.cpp

HEADERS  += mainwindow.h \
            core.h

FORMS    += mainwindow.ui

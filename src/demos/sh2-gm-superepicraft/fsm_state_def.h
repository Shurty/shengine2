/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "GameFrameworkDemo" source code.
// Use, disclosure, copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum FSM_STATE
{
	STATE_LOAD,
	STATE_INIT,
	STATE_INTRO,
	STATE_MAINMENU,
	STATE_INGAME,
	STATE_PAUSE,
	STATE_ENDGAME,
	STATE_EXIT,

	STATEMAX
};
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "GameFrameworkDemo" source code.
// Use, disclosure, copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum PANEL_ID
{
	PANEL_SPLASH,
	PANEL_PAUSE,
	PANEL_HUD,
	PANEL_MENU,
	PANEL_COUNT
};
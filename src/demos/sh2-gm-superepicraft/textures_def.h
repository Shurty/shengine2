/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "GameFrameworkDemo" source code.
// Use, disclosure, copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum TEXID
{

	// ============== UI/HUD ==============
	// Containers
	TEX_UI_POWERUP_BG,

	// Elements
	TEX_UI_HEART_FULL,
	TEX_UI_HEART_EMPTY,

	// ============== Other ==============
	TEX_COUNT
};
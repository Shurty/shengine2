/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

StaticHandler::StaticHandler()
{

}

StaticHandler::~StaticHandler()
{

}

VOID StaticHandler::loadAll(jre2::AssetsEngine* en)
{
	jre2::TextureManager& tex = en->getRenderingEngine()->getTextureManager();
	jre2::FontManager& fnt = en->getRenderingEngine()->getFontManager();

	fnt.loadFont(FONT_DEFAULT, "Data/Fonts/ARIALN.TTF");

	tex.loadTexture(TEX_UI_POWERUP_BG, "Data/Textures/ui_bshape.png");
	tex.loadTexture(TEX_UI_HEART_FULL, "Data/Textures/ui_heart.png");
	tex.loadTexture(TEX_UI_HEART_EMPTY, "Data/Textures/ui_heart_empty.png");

	// Single load your textures here
}
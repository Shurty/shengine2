/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

GameInputsEvents::GameInputsEvents()
{

}

GameInputsEvents::~GameInputsEvents()
{

}

VOID GameInputsEvents::init(jre2::IGameContainer *cnt)
{
	m_gc = cnt;
}

VOID GameInputsEvents::onInputButtonPress(jre2::Event const &ev)
{
	GameData* data = m_gc->getGameData();

	if (ev.key.code == sf::Keyboard::Left)
	{
		data->moveDir |= MOVE_LEFT;
	}
	else if (ev.key.code == sf::Keyboard::Right)
	{
		data->moveDir |= MOVE_RIGHT;
	}
}

VOID GameInputsEvents::onInputButtonRelease(jre2::Event const &ev)
{
	GameData* data = m_gc->getGameData();

	if (ev.key.code == sf::Keyboard::Left)
	{
		data->moveDir &= ~MOVE_LEFT;
	}
	else if (ev.key.code == sf::Keyboard::Right)
	{
		data->moveDir &= ~MOVE_RIGHT;
	}

}

VOID GameInputsEvents::onJoystickMove(jre2::Event const &ev)
{

}

VOID GameInputsEvents::onExitAction(jre2::Event const &ev)
{
	m_gc->getFSM()->changeState(STATE_EXIT);
}

VOID GameInputsEvents::onDevicePreEvent(jre2::Event const &ev)
{

}

VOID GameInputsEvents::onDeviceEvent(jre2::Event const &ev)
{

}

VOID GameInputsEvents::onDevicePostEvent(jre2::Event const &ev)
{

}


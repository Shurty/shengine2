/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class GameImpl : public jre2::IFSMImpl
{
private:
	jre2::IGameContainer* m_activeGc;

public:
	GameImpl();
	virtual ~GameImpl();

	virtual FSM_STATE getInitState();
	virtual VOID load();
	virtual VOID update(jre2::IGameContainer*, FSM_STATE);

	virtual VOID stateLoad(FSM_STATE);
	virtual VOID stateUnload(FSM_STATE);

	virtual VOID renderBegin();
	virtual VOID renderEnd();
	virtual VOID updateBegin();
	virtual VOID updateEnd();

};

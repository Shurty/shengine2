// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <shengine2/shengine2.h>

// ================== GAME PROJECT ==================
// Forced implementations
#include "GameData.hpp"

// Game engine implementations
#include "StaticAssetsHandler.h"
#include "GameImpl.h"
#include "GameInputsEvents.h"

// Connectors
#include "Core.h"

// Game logic - interconnected systems

// Static game stages
#include "GameStage1.h"


// TODO: reference additional headers your program requires here

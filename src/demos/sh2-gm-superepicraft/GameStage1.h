/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class GameStage1 : public jre2::Stage
{
private:
	

public:
	GameStage1();
	virtual ~GameStage1();

	virtual VOID update(jre2::IGameContainer*);
	virtual VOID render(jre2::IGameContainer*);

};
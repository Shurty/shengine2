/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

GameImpl::GameImpl()
{

}

GameImpl::~GameImpl()
{

}

FSM_STATE GameImpl::getInitState()
{
	return (STATE_LOAD);
}

VOID GameImpl::load()
{

}

VOID GameImpl::update(jre2::IGameContainer* gm, FSM_STATE st)
{
	m_activeGc = gm;
	switch (st)
	{
	case STATE_LOAD:
		gm->getFSM()->changeState(STATE_INIT);
		break;
	case STATE_INIT:
		gm->getFSM()->changeState(STATE_INGAME);
		gm->getStageEngine()->setStage((U16)0);
		break;
	case STATE_INGAME:

		updateBegin();
		gm->getInputHandler()->update(gm);
		gm->getStageEngine()->update(gm);
		gm->getGUIController()->update(gm);
		gm->getDebugController()->update(gm);
		updateEnd();

		gm->getRenderingEngine()->setActiveRenderWindow(GAMEWINDOW);
		renderBegin();
		gm->getStageEngine()->render(gm);
		gm->getRenderingEngine()->getRenderWindow().setActiveContext(CONTEXT_UI);
		gm->getGUIController()->render(gm);
		gm->getDebugController()->render(gm);
		gm->getRenderingEngine()->setActiveRenderWindow(GAMEWINDOW);
		gm->getRenderingEngine()->getRenderWindow().setActiveContext(CONTEXT_MAIN);
		renderEnd();

		break;
	default:
		break;
	}
}


VOID GameImpl::stateLoad(FSM_STATE)
{
}

VOID GameImpl::stateUnload(FSM_STATE)
{
}

VOID GameImpl::renderBegin()
{
	jre2::RenderWindow& wnd = m_activeGc->getRenderingEngine()->getRenderWindow();
	jre2::RenderingEngine* rnd = m_activeGc->getRenderingEngine();

	wnd.getWindow().clear(COLOR(0, 0, 0));
}



VOID GameImpl::renderEnd()
{
	m_activeGc->getRenderingEngine()->render();
}

VOID GameImpl::updateBegin()
{
	jre2::RenderWindow& wnd = m_activeGc->getRenderingEngine()->getRenderWindow();
	jre2::RenderingEngine* rnd = m_activeGc->getRenderingEngine();
	jre2::Camera2D* cm = wnd.getCamera();
	GameData* data = m_activeGc->getGameData();

	if (!cm)
		return;
	if (data->moveDir & MOVE_LEFT)
	{
		cm->move(-0.5, 0);
	}
	else if (data->moveDir & MOVE_RIGHT)
	{
		cm->move(0.5, 0);
	}
}

VOID GameImpl::updateEnd()
{
	m_activeGc->getRenderingEngine()->update();
}

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

GameStage1::GameStage1()
{

}

GameStage1::~GameStage1()
{

}


VOID GameStage1::update(jre2::IGameContainer* gc)
{

}

VOID GameStage1::render(jre2::IGameContainer* gc)
{
	jre2::RenderWindow& wnd = gc->getRenderingEngine()->getRenderWindow();
	jre2::RenderingEngine* rnd = gc->getRenderingEngine();

	sf::RectangleShape rect;
	rect.setSize(VECT2F(100, 100));
	rect.setPosition(VECT2F(100, 100));
	rect.setFillColor(COLOR(255, 255, 255));
	wnd.render(rect);
}



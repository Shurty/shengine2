// sh2-gm-superepicraft.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Core core;

	core.load();
	while (core.run() == E_CONTINUE)
		;
	core.exit();

	return 0;
}

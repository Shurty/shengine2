/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

#define MOVE_LEFT	1
#define MOVE_RIGHT	2

struct GameData
{
	GameData()
	{
		moveDir = 0;
	}

	int moveDir;
};
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class GameInputsEvents : public jre2::IEventCallback
{
private:
	jre2::IGameContainer*		m_gc;

public:
	GameInputsEvents();
	virtual ~GameInputsEvents();

	VOID init(jre2::IGameContainer*);

	virtual VOID onInputButtonPress(jre2::Event const &ev);
	virtual VOID onInputButtonRelease(jre2::Event const &ev);
	virtual VOID onJoystickMove(jre2::Event const &ev);

	virtual VOID onExitAction(jre2::Event const &ev);
	virtual VOID onDevicePreEvent(jre2::Event const &ev);
	virtual VOID onDeviceEvent(jre2::Event const &ev);
	virtual VOID onDevicePostEvent(jre2::Event const &ev);

};
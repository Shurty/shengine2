/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

// ================== GLOBAL ==================
// CLibs
#include <iostream>

// CExtLibs

// ================== LIBS ==================
// Libraries
#include <shengine2\shengine2.h>

// ================== ENGINE ==================
// Bootstrap
#include "qtgui.h"


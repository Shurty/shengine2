/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef QTGUI_H
#define QTGUI_H

#include "stdafx.h"

class QtGUI
{
public:
	QtGUI();
	~QtGUI();

private:

};

#endif // QTGUI_H

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
struct EventData
{
	BASICVECT<VOID*> args;
	
	template<typename T>
	T arg(U32 idx)
	{
		if (idx >= args.size())
			idx = 0;
		return ((T)args[idx]);
	}

	SIZET argCount() const
	{
		return (args.size());
	}

	EventData(U32 cnt, ...)
	{
		va_list argslist;
		va_start(argslist, cnt);

		while (cnt)
		{
			VOID* ag;

			ag = va_arg(argslist, VOID*);
			args.push_back(ag);
			--cnt;
		}
		va_end(argslist);
	}
};
__NS_JRE_ENDDEF__
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IGameContainer;
__NS_JRE_ENDDEF__

__NS_JRE_DEF__
class SFMLInput : public IInputConnector
{
private:

public:
	SFMLInput();
	virtual ~SFMLInput();

	virtual VOID load();
	virtual VOID init();
	virtual VOID pollEvents(IGameContainer*, IEventCallback *);
	virtual VOID unload();
};
__NS_JRE_ENDDEF__

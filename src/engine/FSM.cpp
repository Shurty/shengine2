/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
FSM::FSM()
	: m_activeState(STATEMAX), m_futureState(STATEMAX)
{

}

FSM::~FSM()
{

}

VOID FSM::init(IFSMImpl* fsm)
{
	m_fsmImpl = fsm;
	m_fsmImpl->load();
	m_activeState = fsm->getInitState();
}

VOID FSM::update(IGameContainer* cnt)
{
	m_fsmImpl->update(cnt, m_activeState);
	changeStateToFuture();
}

VOID FSM::changeStateToFuture()
{
	if (m_futureState == STATEMAX || m_activeState == m_futureState)
		return;
	if (m_activeState != STATEMAX)
		m_fsmImpl->stateUnload(m_activeState);
	m_activeState = m_futureState;
	m_fsmImpl->stateLoad(m_activeState);
}

VOID FSM::changeState(FSM_STATE st)
{
	if (st == m_activeState)
		return ;
	m_futureState = st;
}

FSM_STATE FSM::getState() const
{
	return (m_activeState);
}
__NS_JRE_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
GameObject::GameObject()
: m_parent(NULL), m_bpname("ent_gameobject")
{

}

GameObject::~GameObject()
{
	if (m_parent) {
		m_parent->removeChild(this);
	}
}

GameObject::GameObject(GameObject const& oth)
{
	this->setEntityBlueprintID(oth.getEntityBlueprintID());
	this->setEntityUID(oth.getEntityUID());
	this->setParent(oth.getParent());
}

IAbstractFactorable* GameObject::getCopy()
{
	return (new GameObject(*this));
}

/////////////////////////////////////////////////////////////////////////////////
// comps.
/////////////////////////////////////////////////////////////////////////////////
VOID GameObject::addComponent(BaseEntityComponent* cmp)
{
	m_components.push_back(cmp);
	cmp->setMaster(this);
	addListener(cmp);
}

VOID GameObject::removeComponent(std::string const &id)
{
	BASICVECT<BaseEntityComponent*>::iterator it = std::find_if(m_components.begin(), m_components.end(), [&id](BaseEntityComponent *comp) {
		if (comp->is(id))
			return (true);
		return (false);
	});

	m_components.erase(it);
}

BaseEntityComponent* GameObject::get(std::string const &id)
{
	BASICVECT<BaseEntityComponent*>::iterator it = std::find_if(m_components.begin(), m_components.end(), [&id](BaseEntityComponent *comp) {
		if (comp->is(id))
			return (true);
		return (false);
	});
	if (it != m_components.end())
	{
		return (*it);
	}
	return (NULL);
}

/////////////////////////////////////////////////////////////////////////////////

VOID GameObject::setParent(GameObject* go)
{
	if (go == NULL)
	{
		m_parent = NULL;
	}
	else
	{
		go->addChild(this);
		m_parent = go;
	}
	updateComponent(EVENT_UPDT_TRANSFORM_PARENT);
}

// logic.
GameObject* GameObject::getParent() const
{
	return (m_parent);
}

SIZET GameObject::getChildrenCount()
{
	return (m_children.size());
}

GameObject* GameObject::getChild(U32 id) const
{
	if (id < 0 || id > m_children.size())
		return (NULL);
	return (m_children[id]);
}

BOOL GameObject::hasChild(GameObject* find)
{
	BASICVECT<GameObject*>::iterator it = std::find_if(m_children.begin(), m_children.end(), [&find](GameObject* go) {
		if (go == find)
			return (true);
		return (false);
	});
	if (*it)
		return (true);
	return (false);
}

VOID GameObject::addChild(GameObject* go)
{
	if (go->getParent() == this)
	{
		return ;
	}
	m_children.push_back(go);
}

VOID GameObject::removeChild(GameObject* go)
{
	std::remove_if(m_children.begin(), m_children.end(), [&go](GameObject *ob) {
		if (ob == go)
		{
			ob->setParent(NULL);
			return (true);
		}
		return (false);
	});
}

VOID GameObject::removeChild(U32 resI)
{
	U32 i;

	i = 0;
	std::remove_if(m_children.begin(), m_children.end(), [&i, &resI](GameObject *ob) {
		if (i == resI)
		{
			ob->setParent(NULL);
			return (true);
		}
		return (false);
	});
}

VOID GameObject::clearChildren()
{
	std::for_each(m_children.begin(), m_children.end(), [](GameObject *ob) {
		ob->setParent(NULL);
	});
	m_children.clear();
}

VOID GameObject::deleteChildren()
{
	std::for_each(m_children.begin(), m_children.end(), [](GameObject *ob) {
		ob->setParent(NULL);
		delete ob;
	});
}

U32 GameObject::getEntityBlueprintID() const
{
	return (m_bpid);
}

STR GameObject::getEntityBlueprintName() const
{
	return (m_bpname);
}

STR const &GameObject::getEntityName() const
{
	return (m_name);
}

U32 GameObject::getEntityUID() const
{
	return (m_uid);
}

VOID GameObject::setEntityBlueprintID(U32 uid)
{
	m_bpid = uid;
}

VOID GameObject::setEntityUID(U32 uid)
{
	m_uid = uid;
}

VOID GameObject::setEntityName(STR const &uid)
{
	m_name = uid;
}
/////////////////////////////////////////////////////////////////////////////////

VOID GameObject::update(IGameContainer*gc)
{
	std::for_each(m_components.begin(), m_components.end(), [&gc](BaseEntityComponent* bc) {
		bc->update(gc);
	});
	std::for_each(m_children.begin(), m_children.end(), [&gc](GameObject* bc) {
		bc->update(gc);
	});
}

VOID GameObject::render(IGameContainer*gc)
{
	std::for_each(m_components.begin(), m_components.end(), [&gc](BaseEntityComponent* bc) {
		bc->render(gc);
	});
	std::for_each(m_children.begin(), m_children.end(), [&gc](GameObject* bc) {
		bc->render(gc);
	});
}

VOID GameObject::updateComponent(EVENT ev)
{
	this->broadcast(ev, EventData(0));
}

VOID GameObject::onFactoryCreate()
{

}

BOOL GameObject::fillFromXML(STR const &comp, tinyxml2::XMLElement* node)
{
	BaseEntityComponent* cmp = get(comp);
	BOOL found = FALSE;
	STR name;

	if (cmp)
	{
		tinyxml2::XMLElement* sub = node->FirstChildElement();

		while (sub)
		{
			name = node->Value();

			if (name == "name")
			{
				setEntityName(node->GetText());
				found = TRUE;
			}
			else
			{
				if (cmp->fillFromXML(sub))
					found = TRUE;
			}
			sub = sub->NextSiblingElement();
		}
	}
	return (found);
}
__NS_JRE_ENDDEF__
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class AssetsEngine;
class FSM;
class RenderingEngine;
class GameClock;
class StageEngine;
class InputHandler;
class Configuration;
class EventsEngine;
class GUIController;
class GameObjectFactory;
class DebugController;
class ComponentsFactory;
__NS_JRE_ENDDEF__

struct GameData;

__NS_JRE_DEF__
class IGameContainer
{
public:
	IGameContainer() {}
	virtual ~IGameContainer() {}

	// Base engines
	virtual AssetsEngine* getAssetsEngine() = 0;
	virtual FSM* getFSM() = 0;
	virtual RenderingEngine* getRenderingEngine() = 0;
	virtual GameClock* getGameClock() = 0;
	virtual StageEngine* getStageEngine() = 0;
	virtual InputHandler* getInputHandler() = 0;
	virtual GameData* getGameData() = 0;
	virtual Configuration* getConfiguration() = 0;
	virtual EventsEngine* getEventsEngine() = 0;
	virtual GUIController* getGUIController() = 0;

	// Debug engines
	virtual DebugController* getDebugController() = 0;

	// Implemented factories
	virtual GameObjectFactory* getGameObjectFactory() = 0;
	virtual ComponentsFactory* getComponentsFactory() = 0;
};
__NS_JRE_ENDDEF__
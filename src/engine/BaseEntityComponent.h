/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_ECS_DEF__
class BaseEntityComponent : public EventListener
{
private:
	GameObject*				m_master;

public:
	BaseEntityComponent();
	BaseEntityComponent(BaseEntityComponent const& oth);
	virtual ~BaseEntityComponent();
	virtual BaseEntityComponent* getCopy() = 0;

	/////////////////////////////////////////////////////////////////////////////////
	// IEntityComponent base
	/////////////////////////////////////////////////////////////////////////////////
	VOID setMaster(GameObject*);
	GameObject* getMaster() const;

	/////////////////////////////////////////////////////////////////////////////////
	// IEntityComponent interface
	/////////////////////////////////////////////////////////////////////////////////
	virtual VOID receive(EVENT ev, EventData const &evData);
	virtual BOOL is(std::string const &id) = 0;
	virtual BOOL fillFromXML(tinyxml2::XMLElement* node) = 0;

	/////////////////////////////////////////////////////////////////////////////////
	// Base connections
	/////////////////////////////////////////////////////////////////////////////////
	virtual VOID update(IGameContainer*);
	virtual VOID render(IGameContainer*);
};
__NS_JRE_ECS_ENDDEF__

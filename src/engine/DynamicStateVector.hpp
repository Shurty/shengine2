/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
template<class T>
class DynamicStateVector
{
private:
	BASICVECT<T>		m_active;
	BASICVECT<T>		m_inswap;
	BASICVECT<T>		m_inactive;

	U32					m_updatedCount;

public:
	DynamicStateVector()
	{

	}
	~DynamicStateVector()
	{

	}

	BOOL pushBack(T res)
	{
		m_inactive.push_back(res);
		return (TRUE);
	}

	BOOL setActive(T res)
	{
		BASICVECT<T>::iterator it = std::find_if(m_inactive.begin(), m_inactive.end(), [&res](T act)
		{
			if (act == res) {
				return (true);
			}
			return (false);
		});
		if (it != m_inactive.end()) {
			m_inswap.push_back(res);
			return (true);
		}
		return (false);
	}

	BOOL setActive(U32 res)
	{
		BASICVECT<T>::iterator it = std::find_if(m_inactive.begin(), m_inactive.end(), [&res](T act)
		{
			if (act->getEntityBlueprintID() == res) {
				return (true);
			}
			return (false);
		});
		if (it != m_inactive.end()) {
			m_inswap.push_back(*it);
			return (true);
		}
		return (false);
	}

	BOOL setInactive(T)
	{
		BASICVECT<T>::iterator it = std::find_if(m_active.begin(), m_active.end(), [&res](T act)
		{
			if (act == res) {
				return (true);
			}
			return (false);
		});
		if (it != m_active.end()) {
			m_inswap.push_back(res);
			return (true);
		}
		return (false);
	}

	BOOL setInactive(U32 res)
	{
		BASICVECT<T>::iterator it = std::find_if(m_active.begin(), m_active.end(), [&res](T act)
		{
			if (act->getEntityBlueprintID() == res) {
				return (true);
			}
			return (false);
		});
		if (it != m_active.end()) {
			m_inswap.push_back(*it);
			return (true);
		}
		return (false);
	}

	BASICVECT<T> const & getActive()
	{
		return (m_active);
	}

	BASICVECT<T> const & getInactive()
	{
		return (m_inactive);
	}

	BASICVECT<T> const & getTemporary()
	{
		return (m_inswap);
	}

	U32 getUpdatedCount()
	{
		return (m_updatedCount);
	}

	VOID update()
	{
		std::for_each(m_inswap.begin(), m_inswap.end(), [=](T swapCur)
		{
			BASICVECT<T>::iterator itActive = std::find_if(m_active.begin(), m_active.end(), [&swapCur](T act)
			{
				if (act == swapCur) {
					return (true);
				}
				return (false);
			});
			BASICVECT<T>::iterator itInactive = std::find_if(m_inactive.begin(), m_inactive.end(), [&swapCur](T act)
			{
				if (act == swapCur) {
					return (true);
				}
				return (false);
			});

			if (itActive != m_active.end()) {
				m_inactive.push_back(*itActive);
				++m_updatedCount;
			}
			else if (itInactive != m_inactive.end()) {
				m_active.push_back(*itInactive);
				++m_updatedCount;
			}
			return (false);
		});
		m_inswap.clear();
	}
};
__NS_JRE_ENDDEF__

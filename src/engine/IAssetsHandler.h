/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class AssetsEngine;
__NS_JRE_ENDDEF__

__NS_JRE_DEF__
class IAssetsHandler
{
public:
	IAssetsHandler() {}
	virtual ~IAssetsHandler() {}

	virtual VOID loadAll(AssetsEngine*) = 0;
};
__NS_JRE_ENDDEF__

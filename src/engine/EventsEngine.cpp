/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
EventsEngine::EventsEngine()
{

}

EventsEngine::~EventsEngine()
{

}

VOID EventsEngine::registerDispatcher(EventBroadcaster* evt)
{
	m_dispatchers.push_back(evt);
}

VOID EventsEngine::broadcast(EVENT ev, EventData const &evData, EVENT_TYPE type)
{
	EVFILTERFUNC tfilter;

	tfilter = [&type](EventBroadcaster* evt) {
		if (evt->hasType(type))
			return (true);
		return (false);
	};
	broadcast(ev, evData, tfilter);
}

VOID EventsEngine::broadcast(EVENT ev, EventData const &evData, EVFILTERFUNC filter)
{
	std::for_each(m_dispatchers.begin(), m_dispatchers.end(), [&ev, &evData, &filter](EventBroadcaster* evt) {
		if (filter && filter(evt)) {
			evt->broadcast(ev, evData);
		}
	});
}
__NS_JRE_ENDDEF__

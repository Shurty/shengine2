/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IGameContainer;
__NS_JRE_ENDDEF__

__NS_JRE_DEF__
class IInputConnector
{
private:

public:
	IInputConnector() {}
	virtual ~IInputConnector() {}

	virtual VOID load() = 0;
	virtual VOID init() = 0;
	virtual VOID pollEvents(IGameContainer*, IEventCallback *) = 0;
	virtual VOID unload() = 0;
};
__NS_JRE_ENDDEF__

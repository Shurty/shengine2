/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_ECS_DEF__
Sprite::Sprite()
: GameObject()
{
	m_bpname = "ent_sprite";

	// Register and add
	addComponent(&m_transform);
	addComponent(&m_renderShape);
}

Sprite::~Sprite()
{

}

IAbstractFactorable* Sprite::getCopy()
{
	return (new Sprite());
}

VOID Sprite::update(IGameContainer* gc)
{
	GameObject::update(gc);
	//RenderObject::update(gc);
}

VOID Sprite::render(IGameContainer* gc)
{
	GameObject::render(gc);
}


BOOL Sprite::fillFromXML(STR const &comp, tinyxml2::XMLElement* node)
{
	return (GameObject::fillFromXML(comp, node));
}

VOID Sprite::updateComponent(EVENT ev)
{
	GameObject::updateComponent(ev);
}
__NS_JRE_ECS_ENDDEF__

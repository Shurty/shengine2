/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

RenderObject::RenderObject()
: GameObject(), m_visible(TRUE), m_textureInit(TRUE)
{

}

RenderObject::~RenderObject()
{

}

RenderObject::RenderObject(RenderObject const& oth)
{
	GameObject::GameObject(oth);
	this->setColor(oth.getColor());
	this->setTexture(oth.getTexture());
	this->setVisible(oth.getVisible());
}

IAbstractFactorable* RenderObject::getCopy()
{
	return (new RenderObject(*this));
}

/////////////////////////////////////////////////////////////////////////////////

VOID RenderObject::setColor(COLOR col)
{
	m_color = col;
	updateRenderData(UPDT_RENDERING_COLOR);
}

VOID RenderObject::setTexture(GTEXTURE* tex)
{
	m_texture = tex;
	updateRenderData(UPDT_RENDERING_TEXTURE);
}

VOID RenderObject::setVisible(BOOL visible)
{
	m_visible = visible;
	updateRenderData(UPDT_RENDERING_VISIBLE);
}

COLOR RenderObject::getColor() const
{
	return (m_color);
}

GTEXTURE* RenderObject::getTexture() const
{
	return (m_texture);
}

BOOL RenderObject::getVisible() const
{
	return (m_visible);
}

VOID RenderObject::update(IGameContainer* gc)
{
	GameObject::update(gc);
	if (!m_textureInit)
	{
		setTexture(gc->getRenderingEngine()->getTextureManager().getTexture(m_tempTextureSrc));
		m_textureInit = true;
	}
}

VOID RenderObject::render(IGameContainer* gc)
{
	GameObject::render(gc);
}

VOID RenderObject::updateRenderData(UPDT_EVENT ev)
{
}

VOID RenderObject::setTextureTempPath(STR const &path)
{
	m_tempTextureSrc = path;
	m_textureInit = FALSE;
}

VOID RenderObject::fillFromXML(tinyxml2::XMLNode* node)
{
	GameObject::fillFromXML(node);

	if (node->FirstChildElement("color"))
	{
		setColor(COLOR(
			strto<U32>(node->FirstChildElement("color")->Attribute("r")),
			strto<U32>(node->FirstChildElement("color")->Attribute("g")),
			strto<U32>(node->FirstChildElement("color")->Attribute("b")),
			strto<U32>(node->FirstChildElement("color")->Attribute("a"))
			));
	}
	if (node->FirstChildElement("texture"))
	{
		const CHAR* tex = node->FirstChildElement("texture")->Attribute("src");
		
		if (tex)
		{
			setTextureTempPath(tex);
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_ECS_DEF__
class GameObjectFactory : public IAbstractEntityFactory<GameObject*>
{
private:
	BASICVECT<GameObject*>		m_blueprints;
	BASICVECT<GameObject*>		m_history;

public:
	GameObjectFactory();
	virtual ~GameObjectFactory();

	virtual VOID addBlueprint(GameObject*);
	virtual VOID removeBlueprint(GameObject*);

	virtual GameObject* forge(STR stid);
	virtual GameObject* forge(U32 stid);

	virtual VOID push(GameObject* obj);
	virtual VOID remove(GameObject* obj);

	BASICVECT<GameObject*>& getHistory();
};
__NS_JRE_ECS_ENDDEF__
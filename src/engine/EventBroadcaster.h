/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class EventBroadcaster
{
private:
	BASICVECT<EVENT_TYPE>		m_types;
	BASICVECT<EventListener*>	m_listeners;

public:
	EventBroadcaster();
	virtual ~EventBroadcaster();

	VOID addListener(EventListener*);
	VOID removeListener(EventListener*);

	BOOL hasType(EVENT_TYPE);
	VOID addType(EVENT_TYPE);

	virtual VOID broadcast(EVENT ev, EventData const &evData);
};
__NS_JRE_ENDDEF__
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class Stage
{
private:
	STR			m_name;

public:
	Stage();
	virtual ~Stage();

	virtual VOID update(IGameContainer*) = 0;
	virtual VOID render(IGameContainer*) = 0;

	VOID setName(STR const &name);
	STR const getName() const;
};
__NS_JRE_ENDDEF__

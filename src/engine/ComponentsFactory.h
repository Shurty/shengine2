/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_ECS_DEF__
class ComponentsFactory : public IAbstractEntityFactory<BaseEntityComponent*>
{
private:
	BASICVECT<BaseEntityComponent*>		m_blueprints;

public:
	ComponentsFactory();
	virtual ~ComponentsFactory();

	virtual VOID addBlueprint(BaseEntityComponent*);
	virtual VOID removeBlueprint(BaseEntityComponent*);

	virtual BaseEntityComponent* forge(STR stid);
	virtual BaseEntityComponent* forge(U32 stid);

	virtual VOID push(BaseEntityComponent* obj);
	virtual VOID remove(BaseEntityComponent* obj);
};
__NS_JRE_ECS_ENDDEF__

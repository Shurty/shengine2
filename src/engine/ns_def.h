/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

#define __NS_JRE__				jre2
#define __NS_JRE_DEF__			namespace __NS_JRE__ {
#define __NS_JRE_ENDDEF__		}

#define __NS_JRE_ECS_NAME__		ecs
#define __NS_JRE_ECS__			__NS_JRE__
#define __NS_JRE_ECS_DEF__		__NS_JRE_DEF__ 
#define __NS_JRE_ECS_ENDDEF__	__NS_JRE_ENDDEF__


#define __XMLDATA_BEGIN__		{
#define __XMLDATA_END__			}
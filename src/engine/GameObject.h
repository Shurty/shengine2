/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IGameContainer;
class BaseEntityComponent;
__NS_JRE_ENDDEF__

__NS_JRE_ECS_DEF__
class GameObject : public IAbstractFactorable, public EventBroadcaster
{
protected:
	// GO.comps impl.
	BASICVECT<BaseEntityComponent*>	m_components;

	// GO.logic impl.
	GameObject*						m_parent;
	BASICVECT<GameObject*>			m_children;

protected:
	// GO.ident impl.
	U32								m_bpid;
	STR								m_bpname;
	U32								m_uid;
	STR								m_name;

public:
	GameObject();
	GameObject(GameObject const&);
	virtual ~GameObject();
	virtual IAbstractFactorable* getCopy();

	/////////////////////////////////////////////////////////////////////////////////
	// comps.
	/////////////////////////////////////////////////////////////////////////////////
	VOID addComponent(BaseEntityComponent*);
	VOID removeComponent(std::string const &id);
	BaseEntityComponent* get(std::string const &id);

	/////////////////////////////////////////////////////////////////////////////////
	// logic.
	/////////////////////////////////////////////////////////////////////////////////
	VOID setParent(GameObject*);
	GameObject* getParent() const;
	SIZET getChildrenCount();
	GameObject* getChild(U32 id) const;
	BOOL hasChild(GameObject*);
	VOID addChild(GameObject*);
	VOID removeChild(GameObject*);
	VOID removeChild(U32);
	VOID clearChildren();
	VOID deleteChildren();

	/////////////////////////////////////////////////////////////////////////////////
	// ident
	/////////////////////////////////////////////////////////////////////////////////
	/*
		Returns the entity blueprint definition ID
		This ID is unique between prototypes but not entities
	*/
	U32 getEntityBlueprintID() const;
	/*
		Returns the entity blueprint definition name id 
		This ID is unique between prototypes but not entities
	*/
	STR getEntityBlueprintName() const;
	/*
		Return the unique identifier ID of the entity amongst instances
		This ID is always unique
	*/
	U32 getEntityUID() const;
	/*
		Return the identifier NAME of the entity amongst instances
	*/
	STR const &getEntityName() const;
	/*
		Sets the entity ID (blueprints)	
	*/
	VOID setEntityBlueprintID(U32 uid);
	/*
		Sets the entity ID (instances UID)
	*/
	VOID setEntityUID(U32 uid);
	/*
		Sets the entity ID (instances UID)
	*/
	VOID setEntityName(STR const &uid);

	/////////////////////////////////////////////////////////////////////////////////
	// abstract methods
	/////////////////////////////////////////////////////////////////////////////////
	virtual VOID update(IGameContainer*);
	virtual VOID updateComponent(EVENT);
	virtual VOID render(IGameContainer*);
	virtual BOOL fillFromXML(STR const &comp, tinyxml2::XMLElement* node);

	virtual VOID onFactoryCreate();

};
__NS_JRE_ECS_ENDDEF__
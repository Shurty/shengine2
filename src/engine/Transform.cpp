/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_ECS_DEF__
Transform::Transform()
: m_parent(NULL)
{

}

Transform::~Transform()
{

}

Transform::Transform(Transform const& oth)
{
	this->setDirection(oth.getDirection());
	this->setPosition(oth.getRelativePosition());
	this->setRotation(oth.getRelativeRotation());
}

BaseEntityComponent* Transform::getCopy()
{
	return (new Transform(*this));
}

/////////////////////////////////////////////////////////////////////////////////

VOID Transform::setMasterParentPtr(GameObject* ptr)
{
	m_parent = ptr;
}

/////////////////////////////////////////////////////////////////////////////////

BOOL Transform::is(std::string const &id)
{
	return (id == "transform");
}

/////////////////////////////////////////////////////////////////////////////////

VOID Transform::move(VECT3F pos)
{
	m_position += pos;
	getMaster()->updateComponent(EVENT_UPDT_TRANSFORM_POSITION);
}

VOID Transform::setPosition(VECT3F pos)
{
	m_position = pos;
	getMaster()->updateComponent(EVENT_UPDT_TRANSFORM_POSITION);
}

VOID Transform::setRotation(VECT3F pos)
{
	m_rotation = pos;
	getMaster()->updateComponent(EVENT_UPDT_TRANSFORM_ANGLE);
}

VOID Transform::setDirection(VECT3F pos)
{
	m_direction = pos;
	getMaster()->updateComponent(EVENT_UPDT_TRANSFORM_DIRECTION);
}


VECT3F Transform::getAbsolutePosition()
{
	if (m_parent)
	{
		Transform* tr = (Transform*)m_parent->get("transform");
		if (tr)
		{
			return (tr->getAbsolutePosition() + m_position);
		}
	}
	return (m_position);
}

VECT3F Transform::getRelativePosition() const
{
	return (m_position);
}

VECT3F Transform::getAbsoluteRotation()
{
	if (m_parent)
	{
		Transform* tr = (Transform*)m_parent->get("transform");
		if (tr)
		{
			return (tr->getAbsoluteRotation() + m_position);
		}
	}
	return (m_rotation);
}

VECT3F Transform::getRelativeRotation() const
{
	return (m_rotation);
}

VECT3F Transform::getDirection() const
{
	return (m_direction);
}

VOID Transform::receive(EVENT ev, EventData const &evData)
{
	switch (ev)
	{
	case EVENT_UPDT_TRANSFORM_PARENT:
	{
		GameObject *parent = getMaster()->getParent();
		setMasterParentPtr(parent);
		break;
	}
	default:
		break;
	}
}

BOOL Transform::fillFromXML(tinyxml2::XMLElement* node)
{
	STR const name = node->Value();

	if (name == "pos")
	{
		setPosition(VECT3F(
			strto<FLOAT>(node->Attribute("x")),
			strto<FLOAT>(node->Attribute("y")),
			strto<FLOAT>(node->Attribute("z"))
			));
		return (TRUE);
	}
	if (name == "rot")
	{
		setRotation(VECT3F(
			strto<FLOAT>(node->Attribute("x")),
			strto<FLOAT>(node->Attribute("y")),
			strto<FLOAT>(node->Attribute("z"))
			));
		return (TRUE);
	}
	if (name == "dir")
	{
		setDirection(VECT3F(
			strto<FLOAT>(node->Attribute("x")),
			strto<FLOAT>(node->Attribute("y")),
			strto<FLOAT>(node->Attribute("z"))
			));
		return (TRUE);
	}
	return (FALSE);
}
__NS_JRE_ECS_ENDDEF__

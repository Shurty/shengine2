/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
Configuration::Configuration(STR src)
: m_src(src)
{

}

Configuration::~Configuration()
{

}

VOID Configuration::load()
{
	if (m_parser.LoadFile(m_src.c_str()) != tinyxml2::XMLError::XML_SUCCESS)
	{
		//Log error;
		return;
	}

	tinyxml2::XMLElement* root = m_parser.FirstChildElement("xml");
	tinyxml2::XMLElement* node = root->FirstChildElement();

	while (node)
	{
		parseNode("config", node);
		node = node->NextSiblingElement();
	}
}

VOID Configuration::parseNode(STR index, tinyxml2::XMLElement* root)
{
	STR const name = root->Value();
	STR const newIndex = index + "." + name;

	if (root->FirstChildElement())
	{
		tinyxml2::XMLElement* node = root->FirstChildElement();

		while (node)
		{
			parseNode(newIndex, node);
			node = node->NextSiblingElement();
		}
	}
	else
	{
		m_map[newIndex] = new ConfigValue(root->GetText());
	}
}

VOID Configuration::save()
{
	
}
__NS_JRE_ENDDEF__
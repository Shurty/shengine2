/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IEventCallback
{

public:
	IEventCallback() { }
	virtual ~IEventCallback() { }

	/*
	Called when a basic INPUT button is pressed for KB/Hardware/MouseButtons devices
	*/
	virtual VOID onInputButtonPress(Event const &ev) = 0;
	/*
	Called when a basic INPUT button is released for KB/Hardware/MouseButtons devices
	*/
	virtual VOID onInputButtonRelease(Event const &ev) = 0;
	/*
	Called when a joystick tracking device / mouse is moved
	*/
	virtual VOID onJoystickMove(Event const &ev) = 0;

	/*
	Called when an event corresponding to a direct exit is called (close target window, DC)
	*/
	virtual VOID onExitAction(Event const &ev) = 0;
	/*
	Called when an event is about to be thrown
	*/
	virtual VOID onDevicePreEvent(Event const &ev) = 0;
	/*
	Called when an event is thrown (includes non-button/joystick events)
	*/
	virtual VOID onDeviceEvent(Event const &ev) = 0;
	/*
	Called right after the event was thrown
	*/
	virtual VOID onDevicePostEvent(Event const &ev) = 0;
};
__NS_JRE_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
SFMLInput::SFMLInput()
: IInputConnector()
{

}

SFMLInput::~SFMLInput()
{

}

VOID SFMLInput::load()
{

}

VOID SFMLInput::init()
{

}

VOID SFMLInput::pollEvents(IGameContainer* gc, IEventCallback* clb)
{
	if (!gc)
		return;

	Event ev;
	GRENDERWND& window = gc->getRenderingEngine()->getRenderWindow().getWindow();

	while (window.pollEvent(ev))
	{
		if (!clb)
			continue;
		clb->onDevicePreEvent(ev);
		clb->onDeviceEvent(ev);
		switch (ev.type)
		{

		// window closed
		case sf::Event::Closed:
			clb->onExitAction(ev);
			break;

		// key pressed
		case sf::Event::KeyPressed:
			clb->onInputButtonPress(ev);
			break;

		// key released
		case sf::Event::KeyReleased:
			clb->onInputButtonRelease(ev);
			break;

		// mouse moved
		case sf::Event::MouseMoved:
			clb->onJoystickMove(ev);
			break;

		// we don't process other types of events
		default:
			break;
		}
		clb->onDevicePostEvent(ev);
	}
}

VOID SFMLInput::unload()
{

}
__NS_JRE_ENDDEF__

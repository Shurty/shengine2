/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
EventListener::EventListener()
{

}

EventListener::~EventListener()
{

}

VOID EventListener::receive(EVENT ev, EventData const &evData)
{

}
__NS_JRE_ENDDEF__

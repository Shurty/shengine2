/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
FontManager::FontManager()
{
}

FontManager::~FontManager()
{

}

VOID FontManager::loadFont(FONTID id, STR path)
{
	m_fonts[id].src = path;
	m_fonts[id].data.loadFromFile(path);
}

GFONT* FontManager::getFont(STR const &src)
{
	for (U32 i = 0; i < FONT_COUNT; i++)
	{
		if (m_fonts[i].src == src)
			return (&m_fonts[i].data);
	}
	return (NULL);
}

GFONT* FontManager::getFont(FONTID id)
{
	if (id > FONT_COUNT || id < 0)
		return (NULL);
	return (&m_fonts[id].data);
}
__NS_JRE_ENDDEF__

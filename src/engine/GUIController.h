/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class GUIController : public EventBroadcaster
{
private:
	//Decorator							m_decorator;
	//Transitor							m_transitor;
	DynamicStateVector<__NS_JRE_ECS__::GameObject*>		m_panels;

public:
	GUIController();
	~GUIController();

	VOID addPanel(__NS_JRE_ECS__::GameObject*);
	VOID setActive(PANEL_ID panel, bool state);

	VOID update(IGameContainer *gc);
	VOID render(IGameContainer *gc);
};
__NS_JRE_ENDDEF__
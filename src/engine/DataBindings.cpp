/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
DataBindings::DataBindings()
{

}

DataBindings::~DataBindings()
{

}

DataBindings::DataBindings(DataBindings const& oth)
{

}

BaseEntityComponent* DataBindings::getCopy()
{
	return (new DataBindings(*this));
}

/////////////////////////////////////////////////////////////////////////////////

BOOL DataBindings::is(std::string const &id)
{
	return (id == "bindings");
}

/////////////////////////////////////////////////////////////////////////////////

VOID DataBindings::receive(EVENT ev, EventData const &evData)
{

}

BOOL DataBindings::fillFromXML(tinyxml2::XMLElement* node)
{
	STR const name = node->Value();

	if (name == "bind")
	{
		m_processingBindings.push_back(node->Attribute("name"));
	}
	return (FALSE);
}

VOID DataBindings::update(IGameContainer* gc)
{
	BaseEntityComponent::update(gc);
	if (m_processingBindings.size() > 0)
	{
		std::for_each(m_processingBindings.begin(), m_processingBindings.end(), [&gc](STR const &msg) {
			std::cout << "DataBindings::update - DUMMY binding processing" << std::endl;
		});
		m_processingBindings.clear();
	}
}
__NS_JRE_ENDDEF__

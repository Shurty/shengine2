/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
RenderShape::RenderShape()
: m_visible(TRUE), m_textureInit(TRUE)
{

}

RenderShape::~RenderShape()
{

}

RenderShape::RenderShape(RenderShape const& oth)
{
	this->setColor(oth.getColor());
	this->setTexture(oth.getTexture());
	this->setVisible(oth.getVisible());
}

BaseEntityComponent* RenderShape::getCopy()
{
	return (new RenderShape(*this));
}

/////////////////////////////////////////////////////////////////////////////////

BOOL RenderShape::is(std::string const &id)
{
	return (id == "render_shape");
}

/////////////////////////////////////////////////////////////////////////////////

VOID RenderShape::setColor(COLOR col)
{
	m_color = col;
	m_shape.setFillColor(m_color);
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_COLOR);
}

VOID RenderShape::setTexture(GTEXTURE* tex)
{
	m_texture = tex;
	m_shape.setTexture(tex, true);
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_TEXTURE);
}

VOID RenderShape::setVisible(BOOL visible)
{
	m_visible = visible;
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_VISIBLE);
}

VOID RenderShape::setSize(VECT2F se)
{
	m_size = se;
	m_shape.setSize(se);
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_SIZE);
}

COLOR RenderShape::getColor() const
{
	return (m_color);
}

GTEXTURE* RenderShape::getTexture() const
{
	return (m_texture);
}

BOOL RenderShape::getVisible() const
{
	return (m_visible);
}

VECT2F RenderShape::getSize() const
{
	return (m_size);
}

VOID RenderShape::update(IGameContainer* gc)
{
	BaseEntityComponent::update(gc);
	if (!m_textureInit)
	{
		setTexture(gc->getRenderingEngine()->getTextureManager().getTexture(m_tempTextureSrc));
		m_textureInit = true;
	}
}

VOID RenderShape::render(IGameContainer* gc)
{
	BaseEntityComponent::render(gc);
	if (m_visible)
		gc->getRenderingEngine()->getRenderWindow().render(m_shape);
}

VOID RenderShape::setTextureTempPath(STR const &path)
{
	m_tempTextureSrc = path;
	m_textureInit = FALSE;
}

VOID RenderShape::receive(EVENT ev, EventData const &evData)
{
	switch (ev)
	{
	case EVENT_UPDT_TRANSFORM_PARENT:
	{
		receive(EVENT_UPDT_TRANSFORM_POSITION, evData);
		receive(EVENT_UPDT_TRANSFORM_ANGLE, evData);
		break;
	}
	case EVENT_UPDT_TRANSFORM_POSITION:
	{
		Transform* tr = (Transform*)getMaster()->get("transform");

		if (tr)
		{
			VECT3F vec = tr->getAbsolutePosition();
			m_shape.setPosition(VECT2F(vec.x, vec.y));
		}
		break;
	}
	case EVENT_UPDT_TRANSFORM_ANGLE:
	{
		Transform* tr = (Transform*)getMaster()->get("transform");

		if (tr)
		{
			m_shape.setRotation(tr->getAbsoluteRotation().x);
		}
		break;
	}
	default:
		break;
	}
}

BOOL RenderShape::fillFromXML(tinyxml2::XMLElement* node)
{
	STR const name = node->Value();

	if (name == "color")
	{
		setColor(COLOR(
			strto<U32>(node->Attribute("r")),
			strto<U32>(node->Attribute("g")),
			strto<U32>(node->Attribute("b")),
			strto<U32>(node->Attribute("a"))
			));
		return (TRUE);
	}
	if (name == "texture")
	{
		const CHAR* tex = node->Attribute("src");

		if (tex)
		{
			setTextureTempPath(tex);
			return (TRUE);
		}
	}
	if (name == "size")
	{
		setSize(VECT2F(
			strto<FLOAT>(node->Attribute("w")),
			strto<FLOAT>(node->Attribute("h"))
			));
		return (TRUE);
	}
	return (FALSE);
}
__NS_JRE_ENDDEF__
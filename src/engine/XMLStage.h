/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IGameContainer;
__NS_JRE_ENDDEF__

__NS_JRE_DEF__
class XMLStage : public Stage
{
private:
	IGameContainer*			m_gc;
	XMLParser				m_parser;
	tinyxml2::XMLElement*	m_hdr;
	tinyxml2::XMLElement*	m_cnt;
	BOOL					m_valid;

	BASICVECT<__NS_JRE_ECS__::GameObject*>	m_entities;

	VOID parseInternal();
	VOID parseEntity(tinyxml2::XMLElement* activeNode, STR const& entName, __NS_JRE_ECS__::GameObject* parent);
	VOID parseEntityDetails(__NS_JRE_ECS__::GameObject* obj, tinyxml2::XMLElement* entityContent);

public:
	XMLStage(IGameContainer*, STR xmlPath);
	virtual ~XMLStage();

	virtual VOID update(IGameContainer*);
	virtual VOID render(IGameContainer*);
};
__NS_JRE_ENDDEF__

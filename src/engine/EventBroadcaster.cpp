/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
EventBroadcaster::EventBroadcaster()
{
}

EventBroadcaster::~EventBroadcaster()
{

}

VOID EventBroadcaster::addListener(EventListener* evl)
{
	m_listeners.push_back(evl);
}

VOID EventBroadcaster::removeListener(EventListener* tevl)
{
	std::remove_if(m_listeners.begin(), m_listeners.end(), [&tevl](EventListener* evl) {
		if (evl == tevl)
			return (true);
		return (false);
	});
}

BOOL EventBroadcaster::hasType(EVENT_TYPE et)
{
	bool exists = false;

	std::for_each(m_types.begin(), m_types.end(), [&exists, &et](EVENT_TYPE type) {
		if (type == et)
			exists = true;
	});
	return (exists);
}

VOID EventBroadcaster::addType(EVENT_TYPE te)
{
	m_types.push_back(te);
}

VOID EventBroadcaster::broadcast(EVENT ev, EventData const &evData)
{
	std::for_each(m_listeners.begin(), m_listeners.end(), [&ev, &evData](EventListener* evl) {
		evl->receive(ev, evData);
	});
}
__NS_JRE_ENDDEF__
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class StageEngine
{
private:
	BASICVECT<Stage*>	m_stages;
	Stage*				m_activeStage;
	Stage*				m_nextStage;

public:
	StageEngine();
	~StageEngine();

	VOID addStage(Stage* stage);

	VOID setStage(U16 id);
	VOID setStage(Stage*);
	Stage* getActiveStage() const;

	VOID init();
	VOID update(IGameContainer*);
	VOID render(IGameContainer*);
};
__NS_JRE_ENDDEF__

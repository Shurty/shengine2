//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IGameContainer;
__NS_JRE_ENDDEF__

__NS_JRE_ECS_DEF__
class Text : public GameObject
{
private:
	//components
	Transform		m_transform;
	RenderLabel		m_renderShape;

	//protected:
	//	// GO.ident impl.
	//	U32								m_bpid;
	//	STR								m_bpname;
	//	U32								m_uid;

public:
	Text();
	virtual ~Text();
	virtual IAbstractFactorable* getCopy();

	virtual VOID update(IGameContainer*);
	virtual VOID updateComponent(EVENT);
	virtual VOID render(IGameContainer*);
	virtual BOOL fillFromXML(STR const &comp, tinyxml2::XMLElement* node);
};
__NS_JRE_ECS_ENDDEF__

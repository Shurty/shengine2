/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class XMLGUI : public __NS_JRE_ECS__::GameObject
{
private:
	IGameContainer*			m_gc;
	XMLParser				m_parser;
	tinyxml2::XMLElement*	m_hdr;
	tinyxml2::XMLElement*	m_cnt;
	BOOL					m_valid;

	//BASICVECT<GameObject*>m_widgets;

	// Orginal parsing - body to subs
	VOID parseInternal();

	// Parsing a node - extract entity
	VOID parseEntity(tinyxml2::XMLElement* activeNode, STR const& entName, __NS_JRE_ECS__::GameObject* parent);

	// Parsing the entity - extract components and children
	VOID parseEntityDetails(__NS_JRE_ECS__::GameObject* obj, tinyxml2::XMLElement* entityContent);

public:
	XMLGUI(IGameContainer*, STR xmlPath);
	virtual ~XMLGUI();

	virtual VOID update(IGameContainer*);
	virtual VOID render(IGameContainer*);
};
__NS_JRE_ENDDEF__

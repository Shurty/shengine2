/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "GameFrameworkDemo" source code.
// Use, disclosure, copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum EVENT
{
	//go.transform
	EVENT_UPDT_TRANSFORM_POSITION,
	EVENT_UPDT_TRANSFORM_ANGLE,
	EVENT_UPDT_TRANSFORM_DIRECTION,
	EVENT_UPDT_TRANSFORM_PARENT,

	//go.rendering
	EVENT_UPDT_RENDERING_POSITION,
	EVENT_UPDT_RENDERING_COLOR,
	EVENT_UPDT_RENDERING_TEXTURE,
	EVENT_UPDT_RENDERING_VISIBLE,
	EVENT_UPDT_RENDERING_TEXT,
	EVENT_UPDT_RENDERING_CHARSIZE,

	//go.in.sprite
	EVENT_UPDT_RENDERING_SIZE,

	EVENT_RENDER,
	EVENT_UPDATE,

	EVENT_COUNT,

};

enum EVENT_TYPE
{
	EVENT_TYPE_CORE,
	EVENT_TYPE_GUI
};
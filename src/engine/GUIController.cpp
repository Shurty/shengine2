/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
GUIController::GUIController()
{

}

GUIController::~GUIController()
{

}

VOID GUIController::addPanel(GameObject* obj)
{
	m_panels.pushBack(obj);
}

VOID GUIController::setActive(PANEL_ID panel, bool state)
{
	if (state) {
		m_panels.setActive(panel);
	}
	else {
		m_panels.setInactive(panel);
	}
}


VOID GUIController::update(IGameContainer *gc)
{
	BASICVECT<GameObject*> panels = m_panels.getActive();

	std::for_each(panels.begin(), panels.end(), [&gc](GameObject* panel) {
		panel->update(gc);
	});
	m_panels.update();
}

VOID GUIController::render(IGameContainer *gc)
{
	BASICVECT<GameObject*> panels = m_panels.getActive();

	std::for_each(panels.begin(), panels.end(), [&gc](GameObject* panel) {
		panel->render(gc);
	});
}
__NS_JRE_ENDDEF__

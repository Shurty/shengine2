/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IAbstractFactorable
{
private:
	BOOL m_factoryDeleteConfirm;

public:
	IAbstractFactorable();
	virtual ~IAbstractFactorable();

	virtual VOID factoryDeleteConfirm();
	virtual VOID onFactoryCreate() = 0;
	virtual IAbstractFactorable* getCopy() = 0;
};
__NS_JRE_ENDDEF__
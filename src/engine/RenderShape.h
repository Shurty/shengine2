/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_ECS_DEF__
class RenderShape : public BaseEntityComponent
{
private:
	// GO.render attributes
	COLOR			m_color;
	GTEXTURE*		m_texture;
	BOOL			m_visible;
	RECTSHAPE		m_shape;
	VECT2F			m_size;

	// internal delayed init system
	BOOL			m_textureInit;
	STR				m_tempTextureSrc;

	VOID			setTextureTempPath(STR const &str);

public:
	RenderShape();
	RenderShape(RenderShape const& oth);
	virtual ~RenderShape();
	virtual BaseEntityComponent* getCopy();

	/////////////////////////////////////////////////////////////////////////////////
	// IEntityComponent interface
	/////////////////////////////////////////////////////////////////////////////////
	virtual BOOL is(std::string const &id);

	/////////////////////////////////////////////////////////////////////////////////
	// render
	/////////////////////////////////////////////////////////////////////////////////
	VOID setColor(COLOR col);
	VOID setTexture(GTEXTURE* tex);
	VOID setVisible(BOOL visible);
	VOID setSize(VECT2F);

	COLOR getColor() const;
	GTEXTURE* getTexture() const;
	BOOL getVisible() const;
	VECT2F getSize() const;

	virtual BOOL fillFromXML(tinyxml2::XMLElement* node);
	
	/////////////////////////////////////////////////////////////////////////////////
	// Base connections
	/////////////////////////////////////////////////////////////////////////////////
	virtual VOID receive(EVENT ev, EventData const &evData);
	virtual VOID update(IGameContainer*);
	virtual VOID render(IGameContainer*);
};
__NS_JRE_ECS_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_ECS_DEF__
class RenderLabel : public BaseEntityComponent
{
private:
	// GO.label attributes
	GFONT*			m_font;
	U32				m_characterSize;
	STR				m_string;
	ALIGN			m_hAlign;
	ALIGN			m_vAlign;
	COLOR			m_color;
	BOOL			m_visible;

	GRECT			m_alignRect;
	
	TEXTSHAPE		m_textShape;

	// internal delayed init system
	BOOL			m_fontInit;
	STR				m_tempFontSrc;

	VOID			setFontTempPath(STR const &str);
	VOID			recalculateTextOrigin();

public:
	RenderLabel();
	RenderLabel(RenderLabel const& oth);
	virtual ~RenderLabel();
	virtual BaseEntityComponent* getCopy();

	/////////////////////////////////////////////////////////////////////////////////
	// IEntityComponent interface
	/////////////////////////////////////////////////////////////////////////////////
	virtual BOOL is(std::string const &id);

	/////////////////////////////////////////////////////////////////////////////////
	// render
	/////////////////////////////////////////////////////////////////////////////////
	VOID setFont(GFONT* font);
	VOID setString(STR str);
	VOID setVAlign(ALIGN d);
	VOID setHAlign(ALIGN d);
	VOID setColor(COLOR col);
	VOID setVisible(BOOL visible);
	VOID setCharacterSize(U32 size);

	GFONT* getFont() const;
	STR getString() const;
	ALIGN getVAlign() const;
	ALIGN getHAlign() const;
	COLOR getColor() const;
	BOOL getVisible() const;
	U32 getCharacterSize() const;

	virtual BOOL fillFromXML(tinyxml2::XMLElement* node);

	/////////////////////////////////////////////////////////////////////////////////
	// Base connections
	/////////////////////////////////////////////////////////////////////////////////
	virtual VOID receive(EVENT ev, EventData const &evData);
	virtual VOID update(IGameContainer*);
	virtual VOID render(IGameContainer*);
};
__NS_JRE_ECS_ENDDEF__

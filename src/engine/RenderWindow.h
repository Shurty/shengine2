/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class Camera2D;
__NS_JRE_ENDDEF__

__NS_JRE_DEF__
class RenderWindow
{
private:
	AWindowRenderer						m_wnd;
	Camera2D*							m_cameras[CONTEXT_COUNT];
	Camera2D*							m_activeCam;

	STR									m_defname;

public:
	RenderWindow();
	~RenderWindow();

	VOID init(STR const &name, IGameContainer *gc);
	VOID create();
	VOID destroy();

	AWindowRenderer& getWindow();

	Camera2D* getCamera() const;
	VOID setActiveContext(RENDERCONTEXT);
	VOID setCamera(RENDERCONTEXT, Camera2D*);

	VOID render(sf::Drawable const &);
};
__NS_JRE_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_ECS_DEF__
class Transform : public BaseEntityComponent
{
private:
	// GO.transform impl.
	VECT3F					m_position;
	VECT3F					m_rotation;
	VECT3F					m_direction;
	GameObject*				m_parent;


public:
	Transform();
	Transform(Transform const& oth);
	virtual ~Transform();
	virtual BaseEntityComponent* getCopy();

	/////////////////////////////////////////////////////////////////////////////////
	// top links
	/////////////////////////////////////////////////////////////////////////////////
	VOID setMasterParentPtr(GameObject* ptr);

	/////////////////////////////////////////////////////////////////////////////////
	// IEntityComponent interface
	/////////////////////////////////////////////////////////////////////////////////
	virtual BOOL is(std::string const &id);

	/////////////////////////////////////////////////////////////////////////////////
	// transform
	/////////////////////////////////////////////////////////////////////////////////
	VOID move(VECT3F pos);
	VOID setPosition(VECT3F pos);
	VOID setRotation(VECT3F pos);
	VOID setDirection(VECT3F pos);
	
	VECT3F getAbsolutePosition();
	VECT3F getRelativePosition() const;
	VECT3F getAbsoluteRotation();
	VECT3F getRelativeRotation() const;
	VECT3F getDirection() const;

	virtual VOID receive(EVENT ev, EventData const &evData);
	virtual BOOL fillFromXML(tinyxml2::XMLElement* node);
};
__NS_JRE_ECS_ENDDEF__

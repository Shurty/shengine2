/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
RenderingEngine::RenderingEngine()
	: m_activeRenderId(0)
{
}

RenderingEngine::~RenderingEngine()
{

}

TextureManager& RenderingEngine::getTextureManager()
{
	return (m_texm);
}

FontManager& RenderingEngine::getFontManager()
{
	return (m_fontm);
}

VOID RenderingEngine::setActiveRenderWindow(U32 id)
{
	m_activeRenderId = id;
}

RenderWindow& RenderingEngine::getRenderWindow() 
{
	return (getRenderWindow(m_activeRenderId));
}

RenderWindow& RenderingEngine::getRenderWindow(U32 id)
{
	if (id < 0)
		id = 0;
	if (id >= m_wnd.size()) 
	{
		m_wnd.push_back(new RenderWindow());
	}
	return (*m_wnd[id]);
}

VOID RenderingEngine::update()
{

}

VOID RenderingEngine::render()
{
	std::for_each(m_wnd.begin(), m_wnd.end(), [=](RenderWindow *wnd) {
		//wnd->getWindow().setView(*wnd->getCamera());
		wnd->getWindow().display();
	});
}
__NS_JRE_ENDDEF__

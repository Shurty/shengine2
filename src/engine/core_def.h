/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum EXECRES
{
	E_CONTINUE,		// Continue execution
	E_EXITOK,		// End execution with ok
	E_ERROR			// End execution with error
};

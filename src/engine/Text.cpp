/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_ECS_DEF__
Text::Text()
: GameObject()
{
	m_bpname = "ent_text";

	// Init components
	m_transform.setMaster(this);
	m_renderShape.setMaster(this);

	// Register and add
	addComponent(&m_transform);
	addComponent(&m_renderShape);
}

Text::~Text()
{

}

IAbstractFactorable* Text::getCopy()
{
	return (new Text());
}

VOID Text::update(IGameContainer* gc)
{
	GameObject::update(gc);
	//RenderObject::update(gc);
}

VOID Text::render(IGameContainer* gc)
{
	GameObject::render(gc);
}


BOOL Text::fillFromXML(STR const &comp, tinyxml2::XMLElement* node)
{
	return (GameObject::fillFromXML(comp, node));
}

VOID Text::updateComponent(EVENT ev)
{
	GameObject::updateComponent(ev);
}
__NS_JRE_ECS_ENDDEF__

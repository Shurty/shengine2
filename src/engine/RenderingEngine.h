/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class RenderingEngine
{
private:
	TextureManager					m_texm;
	FontManager						m_fontm;
	BASICVECT<RenderWindow*>		m_wnd;

	U32								m_activeRenderId;

public:
	RenderingEngine();
	~RenderingEngine();

	VOID setActiveRenderWindow(U32 id);
	TextureManager&	getTextureManager();
	FontManager& getFontManager();
	RenderWindow& getRenderWindow();
	RenderWindow& getRenderWindow(U32 uid);

	VOID update();
	VOID render();
};
__NS_JRE_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_ECS_DEF__
class DataBindings : public BaseEntityComponent
{
private:
	// GO.bindings impl.
	BASICVECT<ConfigValue*>		m_bindings;
	BASICVECT<ConfigValue*>		m_bindingsPrevFrame;

	BASICVECT<STR>				m_processingBindings;

public:
	DataBindings();
	DataBindings(DataBindings const& oth);
	virtual ~DataBindings();
	virtual BaseEntityComponent* getCopy();

	/////////////////////////////////////////////////////////////////////////////////
	// IEntityComponent interface
	/////////////////////////////////////////////////////////////////////////////////
	virtual BOOL is(std::string const &id);

	/////////////////////////////////////////////////////////////////////////////////
	// bindings
	/////////////////////////////////////////////////////////////////////////////////
	VOID bind(ConfigValue &val);

	virtual VOID receive(EVENT ev, EventData const &evData);
	virtual BOOL fillFromXML(tinyxml2::XMLElement* node);
	virtual VOID update(IGameContainer*);
};
__NS_JRE_ECS_ENDDEF__

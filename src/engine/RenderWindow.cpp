/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
RenderWindow::RenderWindow()
	: m_wnd(sf::VideoMode(200, 200), "SFML window"), m_defname("")
{
}

RenderWindow::~RenderWindow()
{

}

VOID RenderWindow::init(STR const &name, IGameContainer *gc)
{
	Configuration* conf = gc->getConfiguration();

	m_defname = name;
	m_wnd.create(sf::VideoMode(
		conf->get<U32>("config.render." + m_defname + ".resolutionX"),
		conf->get<U32>("config.render." + m_defname + ".resolutionY")
	), "STE");
	m_wnd.setTitle(conf->get<STR>("config.render." + m_defname + ".title"));
}

VOID RenderWindow::create()
{
	m_wnd.display();
}

VOID RenderWindow::destroy()
{
	m_wnd.close();
}

GRENDERWND& RenderWindow::getWindow()
{
	return (m_wnd);
}

VOID RenderWindow::setActiveContext(RENDERCONTEXT context)
{
	if (context < 0 || context > CONTEXT_COUNT)
		return;
	m_activeCam = m_cameras[context];
	if (m_activeCam) {
		m_wnd.setView(*m_activeCam);
	}
	else {
		m_wnd.setView(m_wnd.getDefaultView());
	}
}

VOID RenderWindow::setCamera(RENDERCONTEXT context, Camera2D* camera)
{
	m_cameras[context] = camera;
}

Camera2D* RenderWindow::getCamera() const
{
	return (m_activeCam);
}

VOID RenderWindow::render(sf::Drawable const &tr)
{
	m_wnd.draw(tr);
}
__NS_JRE_ENDDEF__

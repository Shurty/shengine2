/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class ConfigValue
{
private:
	STR m_value;

public:
	ConfigValue(STR val);
	~ConfigValue();

	U32 toU32();
	S32 toS32();
	STR toStr();
	FLOAT toFloat();

	template<typename T>
	T to()
	{
		return (strto<T>(m_value));
	}
};
__NS_JRE_ENDDEF__
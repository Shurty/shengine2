/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// Locale for EXECRES
STR EXECRES_STR[] = {
	"E_CONTINUE",
	"E_EXITOK",
	"E_ERROR"
};

// Locale for ALIGN
STR ALIGN_STR[] = {
	"ALIGN_LEFT",
	"ALIGN_CENTER",
	"ALIGN_RIGHT"
};

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
IAbstractFactorable::IAbstractFactorable()
: m_factoryDeleteConfirm(FALSE)
{

}

IAbstractFactorable::~IAbstractFactorable()
{
	if (m_factoryDeleteConfirm == FALSE)
	{
		std::cout << "Non-allowed factory entity destruction" << std::endl;
		//throw new std::runtime_error("Non-allowed factory entity destruction");
	}
}

VOID IAbstractFactorable::factoryDeleteConfirm()
{
	m_factoryDeleteConfirm = TRUE;
}
__NS_JRE_ENDDEF__

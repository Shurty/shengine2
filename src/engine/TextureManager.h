/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
struct GTEXTURESTRUCT
{
	GTEXTURE	data;
	STR			src;
};

class TextureManager
{
private:
	GTEXTURESTRUCT	m_textures[TEX_COUNT];
public:
	TextureManager();
	~TextureManager();

	VOID loadTexture(TEXID, STR path);
	GTEXTURE* getTexture(TEXID id);
	GTEXTURE* getTexture(STR const &path);

};
__NS_JRE_ENDDEF__

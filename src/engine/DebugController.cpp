/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
DebugController::DebugController()
{

}

DebugController::~DebugController()
{

}

VOID DebugController::init(IGameContainer *gc)
{
	m_gc = gc;
	m_debugWnd = &(m_gc->getRenderingEngine()->getRenderWindow());
	m_debugWnd->setActiveContext(CONTEXT_UI);

	// Register debug UI
	XMLGUI* pane = new XMLGUI(gc, "Data/UI/debug.xml");
	m_gui.addPanel(pane);
	m_gui.setActive(PANEL_SPLASH, TRUE);
}

VOID DebugController::update(IGameContainer* gc)
{
	m_gui.update(gc);
}

VOID DebugController::render(IGameContainer* gc)
{
	gc->getRenderingEngine()->setActiveRenderWindow(DEBUGWINDOW);

	RenderWindow& wnd = gc->getRenderingEngine()->getRenderWindow();
	RenderingEngine* rnd = gc->getRenderingEngine();

	wnd.getWindow().clear(COLOR(0, 0, 0));
	m_gui.render(gc);

	BASICVECT<GameObject*>& history = gc->getGameObjectFactory()->getHistory();
	FLOAT ypos = 110;

	std::for_each(history.begin(), history.end(), [&ypos, rnd](GameObject* obj) {
		TEXTSHAPE text;

		text.setString("Gameobject: " + obj->getEntityBlueprintName() + " - " + obj->getEntityName());
		text.setPosition(VECT2F(5, ypos));
		text.setFont(*rnd->getFontManager().getFont(FONT_DEFAULT));
		text.setColor(COLOR(255, 255, 255, 255));
		text.setCharacterSize(12);
		rnd->getRenderWindow().getWindow().draw(text);
		ypos += 15.0;
	});
}
__NS_JRE_ENDDEF__

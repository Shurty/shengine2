/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
AssetsEngine::AssetsEngine()
	: m_handler(NULL)
{

}

AssetsEngine::~AssetsEngine()
{

}

VOID AssetsEngine::load()
{
	if (!m_handler)
		return;
	m_handler->loadAll(this);
}

VOID AssetsEngine::unload()
{

}

VOID AssetsEngine::init(RenderingEngine* render)
{
	m_render = render;
}

RenderingEngine* AssetsEngine::getRenderingEngine() const
{
	return (m_render);
}

IAssetsHandler* AssetsEngine::getHandler()
{
	return (m_handler);
}

VOID AssetsEngine::setHandler(IAssetsHandler*s)
{
	m_handler = s;
}
__NS_JRE_ENDDEF__

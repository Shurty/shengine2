/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IGameContainer;
__NS_JRE_ENDDEF__

__NS_JRE_DEF__
class IFSMImpl
{
public:
	IFSMImpl() {}
	virtual ~IFSMImpl() {}

	virtual FSM_STATE getInitState() = 0;
	virtual VOID load() = 0;
	virtual VOID update(IGameContainer*, FSM_STATE) = 0;

	virtual VOID stateLoad(FSM_STATE) = 0;
	virtual VOID stateUnload(FSM_STATE) = 0;

	virtual VOID renderBegin() = 0;
	virtual VOID renderEnd() = 0;
	virtual VOID updateBegin() = 0;
	virtual VOID updateEnd() = 0;
};
__NS_JRE_ENDDEF__

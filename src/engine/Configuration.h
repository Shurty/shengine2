/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class Configuration
{
private:
	XMLParser					m_parser;
	STR							m_src;

	BASICMAP<STR, ConfigValue*>	m_map;

	VOID parseNode(STR index, tinyxml2::XMLElement* node);

public:
	Configuration(STR xmlPath);
	~Configuration();

	VOID load();
	VOID save();

	template<typename T>
	T get(STR id)
	{
		return (m_map[id]->to<T>());
	}
};
__NS_JRE_ENDDEF__
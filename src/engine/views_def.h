/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "GameFrameworkDemo" source code.
// Use, disclosure, copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

enum WNDID
{
	GAMEWINDOW,
	DEBUGWINDOW,
};


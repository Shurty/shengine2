/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
StageEngine::StageEngine()
	: m_activeStage(NULL)
{

}

StageEngine::~StageEngine()
{

}

VOID StageEngine::addStage(Stage* stage)
{
	m_stages.push_back(stage);
}

Stage* StageEngine::getActiveStage() const
{
	return (m_activeStage);
}

VOID StageEngine::setStage(Stage* st)
{
	if (st == m_activeStage)
		return;
	m_nextStage = st;
}

VOID StageEngine::setStage(U16 id)
{
	if (id < 0 || id > m_stages.size())
		return;
	setStage(m_stages[id]);
}

VOID StageEngine::init()
{
	m_activeStage = NULL;
}

VOID StageEngine::update(IGameContainer *gc)
{
	if (m_activeStage)
		m_activeStage->update(gc);
	if (m_nextStage) {
		m_activeStage = m_nextStage;
	}
}

VOID StageEngine::render(IGameContainer *gc)
{
	if (m_activeStage)
		m_activeStage->render(gc);
}
__NS_JRE_ENDDEF__




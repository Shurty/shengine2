/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_ECS_DEF__
ComponentsFactory::ComponentsFactory()
{

}

ComponentsFactory::~ComponentsFactory()
{

}

VOID ComponentsFactory::addBlueprint(BaseEntityComponent* ob)
{
	m_blueprints.push_back(ob);
}

VOID ComponentsFactory::removeBlueprint(BaseEntityComponent* ob)
{
	std::remove_if(m_blueprints.begin(), m_blueprints.end(), [&ob](BaseEntityComponent* obj) {
		if (ob == obj)
			return (true);
		return (false);
	});
}

BaseEntityComponent* ComponentsFactory::forge(STR stid)
{
	BASICVECT<BaseEntityComponent*>::iterator it = std::find_if(m_blueprints.begin(), m_blueprints.end(), [&stid](BaseEntityComponent* obj) {
		if (obj->is(stid))
			return (true);
		return (false);
	});
	if (it == m_blueprints.end())
		return (NULL);
	return (dynamic_cast<BaseEntityComponent*>((*it)->getCopy()));
}

BaseEntityComponent* ComponentsFactory::forge(U32 stid)
{
	if (stid > m_blueprints.size())
		return (NULL);
	return (dynamic_cast<BaseEntityComponent*>((m_blueprints[stid])->getCopy()));
}

VOID ComponentsFactory::push(BaseEntityComponent* obj)
{

}

VOID ComponentsFactory::remove(BaseEntityComponent* obj)
{

}
__NS_JRE_ECS_ENDDEF__

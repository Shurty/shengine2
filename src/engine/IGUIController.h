/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IGUIController
{
public:
	IGUIController() { }
	virtual ~IGUIController() { }

	virtual VOID addPanel(__NS_JRE_ECS__::GameObject*) = 0;
	virtual VOID setActive(PANEL_ID panel, bool state) = 0;

	virtual VOID update(IGameContainer *gc) = 0;
	virtual VOID render(IGameContainer *gc) = 0;
};
__NS_JRE_ENDDEF__
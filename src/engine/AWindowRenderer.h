/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class Camera2D;
__NS_JRE_ENDDEF__

__NS_JRE_DEF__
class AWindowRenderer
{
private:


public:
	virtual void Create(int width, int height) = 0;
	virtual void Close() = 0;

	virtual void SetView(GRENDERVIEW view) = 0;
	virtual GRENDERVIEW GetDefaultView() = 0;
	virtual void SetTitle(STR title) = 0;

	AWindowRenderer();
	virtual ~AWindowRenderer();

	virtual void OnInit();
	virtual void OnUpdate();

};
__NS_JRE_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
RenderLabel::RenderLabel()
: m_font(NULL), m_hAlign(ALIGN_LEFT), m_vAlign(ALIGN_LEFT),
	m_characterSize(12), m_color(255, 255, 255, 255), m_visible(TRUE)
{

}

RenderLabel::~RenderLabel()
{

}

RenderLabel::RenderLabel(RenderLabel const& oth)
: m_font(NULL), m_hAlign(ALIGN_LEFT), m_vAlign(ALIGN_LEFT),
	m_characterSize(12), m_color(255, 255, 255, 255), m_visible(TRUE)
{
	//this->setMaster(oth.getMaster());
	//this->setHAlign(oth.getHAlign());
	//this->setVAlign(oth.getVAlign());
	//this->setString(oth.getString());
	//this->setFont(oth.getFont());
}

BaseEntityComponent* RenderLabel::getCopy()
{
	return (new RenderLabel(*this));
}

/////////////////////////////////////////////////////////////////////////////////

BOOL RenderLabel::is(std::string const &id)
{
	return (id == "render_label");
}

/////////////////////////////////////////////////////////////////////////////////

VOID RenderLabel::recalculateTextOrigin()
{
	GFRECT rect = m_textShape.getLocalBounds();
	FLOAT wPos = rect.left;
	FLOAT hPos = rect.top;

	if (m_hAlign == ALIGN_CENTER) {
		wPos += rect.width / 2;
	}
	else if (m_hAlign == ALIGN_RIGHT) {
		wPos += rect.width;
	}
	if (m_vAlign == ALIGN_CENTER) {
		hPos += rect.width / 2;
	}
	else if (m_vAlign == ALIGN_RIGHT) {
		hPos += rect.width;
	}
	m_textShape.setOrigin(VECT2F(wPos, hPos));
}

VOID RenderLabel::setVAlign(ALIGN d)
{
	m_vAlign = d;
	recalculateTextOrigin();
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_POSITION);
}

VOID RenderLabel::setHAlign(ALIGN d)
{
	m_hAlign = d;
	recalculateTextOrigin();
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_POSITION);
}

VOID RenderLabel::setString(STR str)
{
	m_string = str;
	m_textShape.setString(str);
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_TEXT);
}

VOID RenderLabel::setColor(COLOR col)
{
	m_color = col;
	m_textShape.setColor(m_color);
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_COLOR);
}

VOID RenderLabel::setFont(GFONT* tex)
{
	m_font = tex;
	m_textShape.setFont(*tex);
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_TEXTURE);
}

VOID RenderLabel::setVisible(BOOL visible)
{
	m_visible = visible;
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_VISIBLE);
}

VOID RenderLabel::setCharacterSize(U32 sw)
{
	m_characterSize = sw;
	m_textShape.setCharacterSize(sw);
	getMaster()->updateComponent(EVENT_UPDT_RENDERING_CHARSIZE);
}

ALIGN RenderLabel::getHAlign() const
{
	return (m_hAlign);
}

ALIGN RenderLabel::getVAlign() const
{
	return (m_vAlign);
}

STR RenderLabel::getString() const
{
	return (m_string);
}

GFONT* RenderLabel::getFont() const
{
	return (m_font);
}

COLOR RenderLabel::getColor() const
{
	return (m_color);
}

BOOL RenderLabel::getVisible() const
{
	return (m_visible);
}

U32 RenderLabel::getCharacterSize() const
{
	return (m_characterSize);
}

VOID RenderLabel::update(IGameContainer* gc)
{
	BaseEntityComponent::update(gc);
	if (!m_fontInit)
	{
		setFont(gc->getRenderingEngine()->getFontManager().getFont(m_tempFontSrc));
		m_fontInit = true;
	}
}

VOID RenderLabel::render(IGameContainer* gc)
{
	BaseEntityComponent::render(gc);
	if (m_visible) {
		gc->getRenderingEngine()->getRenderWindow().render(m_textShape);
	}
}

VOID RenderLabel::setFontTempPath(STR const &path)
{
	m_tempFontSrc = path;
	m_fontInit = FALSE;
}

VOID RenderLabel::receive(EVENT ev, EventData const &evData)
{
	switch (ev)
	{
	case EVENT_UPDT_TRANSFORM_PARENT:
	{
										receive(EVENT_UPDT_TRANSFORM_POSITION, evData);
										receive(EVENT_UPDT_TRANSFORM_ANGLE, evData);
										break;
	}
	case EVENT_UPDT_TRANSFORM_POSITION:
	{
										  Transform* tr = (Transform*)getMaster()->get("transform");

										  if (tr)
										  {
											  VECT3F vec = tr->getAbsolutePosition();
											  m_textShape.setPosition(VECT2F(vec.x, vec.y));
										  }
										  break;
	}
	case EVENT_UPDT_TRANSFORM_ANGLE:
	{
									   Transform* tr = (Transform*)getMaster()->get("transform");

									   if (tr)
									   {
										   m_textShape.setRotation(tr->getAbsoluteRotation().x);
									   }
									   break;
	}
	default:
		break;
	}
}

BOOL RenderLabel::fillFromXML(tinyxml2::XMLElement* node)
{
	STR const name = node->Value();

	if (name == "color")
	{
		setColor(COLOR(
			strto<U32>(node->Attribute("r")),
			strto<U32>(node->Attribute("g")),
			strto<U32>(node->Attribute("b")),
			strto<U32>(node->Attribute("a"))
			));
		return (TRUE);
	}
	if (name == "align")
	{
		setVAlign((ALIGN)strto<U32>(node->Attribute("v")));
		setHAlign((ALIGN)strto<U32>(node->Attribute("h")));
		return (TRUE);
	}
	if (name == "font")
	{
		const CHAR* tex = node->Attribute("src");

		if (tex)
		{
			setFontTempPath(tex);
			return (TRUE);
		}
	}
	if (name == "text")
	{
		setCharacterSize(strto<U32>(node->Attribute("size")));
		setString(node->Attribute("val"));
		return (TRUE);
	}
	return (FALSE);
}
__NS_JRE_ENDDEF__
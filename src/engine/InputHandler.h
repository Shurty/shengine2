/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class IGameContainer;
__NS_JRE_ENDDEF__

__NS_JRE_DEF__
class InputHandler
{
private:
	BASICVECT<IInputConnector*>	m_inputs;
	IEventCallback*				m_clb;

public:
	InputHandler();
	~InputHandler();

	VOID add(IInputConnector*);

	VOID init(IEventCallback*);
	VOID update(IGameContainer*);
};
__NS_JRE_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class FSM
{
private:
	FSM_STATE	m_activeState;
	FSM_STATE	m_futureState;
	IFSMImpl*	m_fsmImpl;

	VOID changeStateToFuture();

public:
	FSM();
	~FSM();

	VOID init(IFSMImpl*);
	VOID update(IGameContainer*);

	VOID changeState(FSM_STATE);
	FSM_STATE getState() const;
};
__NS_JRE_ENDDEF__
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
ConfigValue::ConfigValue(STR val)
: m_value(val)
{

}

ConfigValue::~ConfigValue()
{

}
U32 ConfigValue::toU32()
{
	return (strto<U32>(m_value));
}

S32 ConfigValue::toS32()
{
	return (strto<S32>(m_value));
}

STR ConfigValue::toStr()
{
	return (strto<STR>(m_value));
}

FLOAT ConfigValue::toFloat()
{
	return (strto<FLOAT>(m_value));
}
__NS_JRE_ENDDEF__


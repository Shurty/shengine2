/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "GameFrameworkDemo" source code.
// Use, disclosure, copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef __SHENGINE2_H__
#define __SHENGINE2_H__

// ================== GLOBAL ==================
// VS
#include "targetver.h"

// CLibs
#include <stdio.h>
#include <tchar.h>

// CExtLibs
#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>
#include <functional>

// ================== UTILS ==================
template<typename T>
T strto(std::string const &str)
{
	std::stringstream ss(str);;
	T res;

	ss >> res;
	return (res);
}

// ================== LIBS ==================
// Libraries
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <tinyxml2.h>
#include <Windows.h>

// ================== GAME ENGINE ==================
// Defines
#include "type_def.h"
#include "core_def.h"
#include "ns_def.h"
#include "align_def.h"

// "Dynamic" Defines
#include "textures_def.h"
#include "fonts_def.h"
#include "fsm_state_def.h"
#include "updt_elem_def.h"
#include "views_def.h"
#include "event_def.h"
#include "panel_def.h"
#include "rendercontext_def.h"

// Base objects/entities
#include "EventData.h"
#include "EventListener.h"
#include "EventBroadcaster.h"
#include "AbstractFactorable.h"
#include "GameObject.h"
#include "ConfigValue.h"
#include "DynamicStateVector.hpp"
#include "BaseEntityComponent.h"

// ECS - DDECS Components
#include "Transform.h"
#include "RenderShape.h"
#include "RenderLabel.h"
#include "DataBindings.h"

// Entities
#include "Sprite.h"
#include "Text.h"

// Engines Submodules
#include "TextureManager.h"
#include "FontManager.h"
#include "RenderWindow.h"
#include "IAssetsHandler.h"
#include "IFSMImpl.h"
#include "IGameContainer.h"
#include "XMLParser.h"
#include "Stage.h"
#include "XMLStage.h"
#include "Camera2D.h"
#include "Event.h"
#include "IEventCallback.h"
#include "IInputConnector.h"
#include "IAbstractEntityFactory.h"
#include "XMLGui.h"

// Engines default implementations
#include "SFMLInput.h"
#include "GameObjectFactory.h"
#include "ComponentsFactory.h"

// Engines
#include "GameClock.h"
#include "RenderingEngine.h"
#include "AssetsEngine.h"
#include "FSM.h"
#include "StageEngine.h"
#include "InputHandler.h"
#include "Configuration.h"
#include "EventsEngine.h"
#include "GUIController.h"
#include "DebugController.h"

// Modules

// Submodules

#endif // __SHENGINE2_H__
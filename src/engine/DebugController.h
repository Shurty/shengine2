/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
class DebugController : public EventBroadcaster
{
private:
	IGameContainer*			m_gc;
	RenderWindow*			m_debugWnd;
	GUIController			m_gui;

public:
	DebugController();
	~DebugController();

	VOID init(IGameContainer *gc);

	VOID update(IGameContainer* gc);
	VOID render(IGameContainer* gc);
};
__NS_JRE_ENDDEF__
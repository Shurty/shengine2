/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
struct GFONTSTRUCT
{
	GFONT		data;
	STR			src;
};

class FontManager
{
private:
	GFONTSTRUCT		m_fonts[FONT_COUNT];
public:
	FontManager();
	~FontManager();

	VOID loadFont(FONTID, STR path);
	GFONT *getFont(FONTID id);
	GFONT *getFont(STR const &path);

};
__NS_JRE_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_ECS_DEF__
GameObjectFactory::GameObjectFactory()
{ 

}

GameObjectFactory::~GameObjectFactory()
{ 

}

VOID GameObjectFactory::addBlueprint(GameObject* ob)
{
	m_blueprints.push_back(ob);
}

VOID GameObjectFactory::removeBlueprint(GameObject* ob)
{
	std::remove_if(m_blueprints.begin(), m_blueprints.end(), [&ob](GameObject* obj) {
		if (ob == obj)
			return (true);
		return (false);
	});
}

GameObject* GameObjectFactory::forge(STR stid)
{
	BASICVECT<GameObject*>::iterator it = std::find_if(m_blueprints.begin(), m_blueprints.end(), [&stid](GameObject* obj) {
		if (obj->getEntityBlueprintName() == stid)
			return (true);
		return (false);
	});
	GameObject* res;

	if (it == m_blueprints.end())
		return (NULL);
	res = dynamic_cast<GameObject*>((*it)->getCopy());
	push(res);
	return (res);
}

GameObject* GameObjectFactory::forge(U32 stid)
{
	BASICVECT<GameObject*>::iterator it = std::find_if(m_blueprints.begin(), m_blueprints.end(), [&stid](GameObject* obj) {
		if (obj->getEntityBlueprintID() == stid)
			return (true);
		return (false);
	});
	GameObject* res;

	if (it == m_blueprints.end())
		return (NULL);
	res = dynamic_cast<GameObject*>((*it)->getCopy());
	push(res);
	return (res);
}

VOID GameObjectFactory::push(GameObject* obj)
{
	m_history.push_back(obj);
}

VOID GameObjectFactory::remove(GameObject* obj)
{
	std::remove_if(m_history.begin(), m_history.end(), [&obj](GameObject* res) {
		if (res == obj)
			return true;
		return false;
	});
	delete obj;
}

BASICVECT<GameObject*>& GameObjectFactory::getHistory()
{
	return (m_history);
}
__NS_JRE_ECS_ENDDEF__

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{

}

VOID TextureManager::loadTexture(TEXID id, STR path)
{
	m_textures[id].src = path;
	m_textures[id].data.loadFromFile(path);
}

GTEXTURE* TextureManager::getTexture(TEXID id)
{
	if (id > TEX_COUNT || id < 0)
		return (NULL);
	return (&m_textures[id].data);
}

GTEXTURE* TextureManager::getTexture(STR const &src)
{
	for (U32 i = 0; i < TEX_COUNT; i++)
	{
		if (m_textures[i].src == src)
			return (&m_textures[i].data);
	}
	return (NULL);
}
__NS_JRE_ENDDEF__
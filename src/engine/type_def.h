/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

// Basictypes
#ifdef _WIN32
typedef std::string			STR;	// nxoct
#ifndef VOID
typedef void				VOID;	// ;
#endif
typedef int					S32;	// 4o-
typedef unsigned int		U32;	// 4o
typedef short				S16;	// 2o-
typedef unsigned short		U16;	// 2o
typedef char				CHAR;	// 1o
typedef float				FLOAT;	// 4o-
typedef size_t				SIZET;	// 8o
//#ifndef BOOL
//typedef bool				BOOL;	// 1o
//#endif
typedef U32					UINT;	// alias
//#define TRUE true
//#define FALSE false
#else
/* Define non WIN32 basetypes here */
#endif

// RenderDef
#ifdef _SFML
typedef sf::Font			GFONT;
typedef sf::Texture			GTEXTURE;
typedef sf::RenderWindow	GRENDERWND;
typedef sf::View			GRENDERVIEW;
typedef sf::Color			COLOR;
typedef sf::IntRect			GRECT;
typedef sf::FloatRect		GFRECT;
// RenderDef - SFML Shapes
typedef sf::RectangleShape	RECTSHAPE;
typedef sf::Text			TEXTSHAPE;
template<typename T>
using _VECT2 = sf::Vector2<T>;
typedef _VECT2<FLOAT>		VECT2F;
typedef _VECT2<S32>			VECT2;
typedef _VECT2<U32>			VECT2U;
template<typename T>
using _VECT3 = sf::Vector3<T>;
typedef _VECT3<FLOAT>		VECT3F;
typedef _VECT3<S32>			VECT3;
typedef _VECT3<U32>			VECT3U;

#else
/* Define non SFML rendering classes here */
#endif

// Containers
#ifdef _WIN32
#ifdef _USE_STL
template<typename T, typename U>
using BASICMAP = std::map<T, U>;
template<typename T>
using BASICVECT = std::vector<T>;
#else
/* Define local implementation of stl containers here */
#endif
#else
/* Define non-win32 implementation of stl containers here */
#endif
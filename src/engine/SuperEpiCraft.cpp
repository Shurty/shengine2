/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "GameFrameworkDemo" source code.
// Use, disclosure, copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Core core;

	core.load();
	while (core.run() == E_CONTINUE)
		;
	core.exit();

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
template<class T>
class IAbstractEntityFactory
{
private:

public:
	IAbstractEntityFactory() { }
	virtual ~IAbstractEntityFactory() { }

	virtual VOID addBlueprint(T) = 0;
	virtual VOID removeBlueprint(T) = 0;

	virtual T forge(STR stid) = 0;
	virtual T forge(U32 stid) = 0;

	virtual VOID push(T obj) = 0;
	virtual VOID remove(T obj) = 0;
};
__NS_JRE_ENDDEF__
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
Stage::Stage()
: m_name("")
{

}

Stage::~Stage()
{

}

VOID Stage::setName(STR const &name) { m_name = name; }
STR const Stage::getName() const { return (m_name); }
__NS_JRE_ENDDEF__


/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

class IGameContainer;

class RenderObject : public GameObject
{
protected:
	// Inheriting GO.transforms

	// GO.render attributes
	COLOR			m_color;
	GTEXTURE*		m_texture;
	BOOL			m_visible;

	// internal delayed init system
	BOOL			m_textureInit;
	STR				m_tempTextureSrc;

	VOID setTextureTempPath(STR const &str);

public:
	RenderObject();
	RenderObject(RenderObject const&);
	virtual ~RenderObject();
	virtual IAbstractFactorable* getCopy();

	VOID setColor(COLOR col);
	VOID setTexture(GTEXTURE* tex);
	VOID setVisible(BOOL visible);

	COLOR getColor() const;
	GTEXTURE* getTexture() const;
	BOOL getVisible() const;

	virtual VOID update(IGameContainer*);
	virtual VOID render(IGameContainer*);

	virtual VOID fillFromXML(tinyxml2::XMLNode* node);
	virtual VOID updateRenderData(UPDT_EVENT);
};
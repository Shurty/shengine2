/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
InputHandler::InputHandler()
: m_clb(NULL)
{

}

InputHandler::~InputHandler()
{
	// Internal cleanup of remaining input systems
	std::for_each(m_inputs.begin(), m_inputs.end(), [=](IInputConnector* itm) {
		itm->unload();
		delete itm;
	});
	m_inputs.erase(m_inputs.begin(), m_inputs.end());
}

VOID InputHandler::add(IInputConnector* itn)
{
	m_inputs.push_back(itn);
}

VOID InputHandler::init(IEventCallback* clb)
{
	m_clb = clb;
	std::for_each(m_inputs.begin(), m_inputs.end(), [=](IInputConnector* itm) {
		itm->load();
		itm->init();
	});
}

VOID InputHandler::update(IGameContainer *gc)
{
	std::for_each(m_inputs.begin(), m_inputs.end(), [=, &gc](IInputConnector* itm) {
		itm->pollEvents(gc, m_clb);
	});
}
__NS_JRE_ENDDEF__

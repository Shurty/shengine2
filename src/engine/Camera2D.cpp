/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
Camera2D::Camera2D()
	: sf::View()
{

}

Camera2D::~Camera2D()
{

}

VOID Camera2D::setupFullsize(RenderWindow *wnd)
{
	setCenter((float)wnd->getWindow().getSize().x / 2.0f, (float)wnd->getWindow().getSize().y / 2.0f);
	setSize((float)wnd->getWindow().getSize().x, (float)wnd->getWindow().getSize().y);
}
__NS_JRE_ENDDEF__

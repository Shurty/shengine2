/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
XMLGUI::XMLGUI(IGameContainer* gc, STR xmlPath)
: GameObject(), m_gc(gc), m_parser(), m_valid(false)
{
	m_bpname = "ui_xmlgui";
	if (m_parser.LoadFile(xmlPath.c_str()) != tinyxml2::XMLError::XML_SUCCESS)
	{
		std::cout << "XMLGUI::XMLGUI(" << (unsigned long long)gc << ", " << xmlPath << ") Loading failed" << std::endl;
		//Log error;
		return;
	}
	m_valid = true;
	parseInternal();
}

VOID XMLGUI::parseInternal()
{
	// Get hdr data
	tinyxml2::XMLElement* root = m_parser.FirstChildElement("xml");
	m_hdr = root->FirstChildElement("head");

	// Get contents
	m_cnt = root->FirstChildElement("content");
	tinyxml2::XMLElement* node = m_cnt->FirstChildElement();

	// Render DD-ECS entities
	while (node)
	{
		STR const name = node->Value();

		parseEntity(node, name, NULL);
		node = node->NextSiblingElement();
	}
}

VOID XMLGUI::parseEntity(tinyxml2::XMLElement* activeNode, STR const& entName, GameObject* parent)
{
	GameObject* obj = m_gc->getGameObjectFactory()->forge(entName);

	// Check if it's a gameobject
	if (obj)
	{
		if (parent) {
			// A sub object is a child of entity
			obj->setParent(parent);
		}
		else {
			// No sub object - this is the parent
			obj->setParent(this);
		}
		parseEntityDetails(obj, activeNode);
		//m_widgets.push_back(obj);
	}
	// Check if it's an unregistered added component to the entity
	else if (parent)
	{
		BaseEntityComponent* cp = m_gc->getComponentsFactory()->forge(entName);
		if (cp)
		{
			parent->addComponent(cp);
			parent->fillFromXML(entName, activeNode);
		}
	}
}

VOID XMLGUI::parseEntityDetails(GameObject* obj, tinyxml2::XMLElement* entityContent)
{
	STR activeComp = "";
	tinyxml2::XMLElement* node = entityContent->FirstChildElement();

	// Render cosmetic elements (background base and non interacting elements)
	while (node)
	{
		STR const name = node->Value();

		if (obj->get(name))
		{
			if (obj->fillFromXML(name, node) == FALSE)  {
				// The component has no attributes set
				;
			}
		}
		else
		{
			if (obj->fillFromXML("", node) == FALSE) {
				parseEntity(node, name, obj);
			}
		}
		node = node->NextSiblingElement();
	}
}

XMLGUI::~XMLGUI()
{

}

VOID XMLGUI::update(IGameContainer* gc)
{
	std::for_each(m_children.begin(), m_children.end(), [=, &gc](GameObject *rect) {
		rect->update(gc);
	});
}

VOID XMLGUI::render(IGameContainer* gc)
{
	std::for_each(m_children.begin(), m_children.end(), [=, &gc](GameObject *rect) {
		rect->render(gc);
	});
}
__NS_JRE_ENDDEF__

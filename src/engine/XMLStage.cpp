/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

__NS_JRE_DEF__
XMLStage::XMLStage(IGameContainer* gc, STR xmlPath)
: m_gc(gc), m_parser(), m_valid(false)
{
	if (m_parser.LoadFile(xmlPath.c_str()) != tinyxml2::XMLError::XML_SUCCESS)
	{
		//Log error;
		return;
	}
	m_valid = true;
	parseInternal();
}

VOID XMLStage::parseInternal()
{
	// Get hdr data
	tinyxml2::XMLElement* root = m_parser.FirstChildElement("xml");
	m_hdr = root->FirstChildElement("head");
	setName(m_hdr->FirstChildElement("name")->GetText());

	// Get contents
	m_cnt = root->FirstChildElement("content");
	tinyxml2::XMLElement* node = m_cnt->FirstChildElement();

	// Render DD-ECS entities
	while (node)
	{
		STR const name = node->Value();

		parseEntity(node, name, NULL);
		node = node->NextSiblingElement();
	}
}

VOID XMLStage::parseEntity(tinyxml2::XMLElement* activeNode, STR const& entName, __NS_JRE_ECS__::GameObject* parent)
{
	__NS_JRE_ECS__::GameObject* obj = m_gc->getGameObjectFactory()->forge(entName);

	// Check if it's a gameobject
	if (obj)
	{
		if (parent) {
			obj->setParent(parent);
		}
		parseEntityDetails(obj, activeNode);
		m_entities.push_back(obj);
	}
	// Check if it's an unregistered added component to the entity
	else if (parent) 
	{
		BaseEntityComponent* cp = m_gc->getComponentsFactory()->forge(entName);
		if (cp)
		{
			parent->addComponent(cp);
			parent->fillFromXML(entName, activeNode);
		}
	}
}

VOID XMLStage::parseEntityDetails(__NS_JRE_ECS__::GameObject* obj, tinyxml2::XMLElement* entityContent)
{
	STR activeComp = "";
	tinyxml2::XMLElement* node = entityContent->FirstChildElement();

	// Render cosmetic elements (background base and non interacting elements)
	while (node)
	{
		STR const name = node->Value();

		if (obj->get(name))
		{
			if (obj->fillFromXML(name, node) == FALSE) {
				// The component has no attributes set
				;
			}
		}
		else
		{
			if (obj->fillFromXML("", node) == FALSE) {
				// Means we didn't find a registered component, and is not a entity-specific atribute
				// It can be :
				// - An unregistered componnent
				// - An invalid attribute
				// - A subentity/child
				parseEntity(node, name, obj);
			}
		}
		node = node->NextSiblingElement();
	}
}

XMLStage::~XMLStage()
{

}

VOID XMLStage::update(IGameContainer* gc)
{
	std::for_each(m_entities.begin(), m_entities.end(), [=, &gc](__NS_JRE_ECS__::GameObject *rect) {
		if (rect->getParent() == NULL)
			rect->update(gc);
	});
}

VOID XMLStage::render(IGameContainer* gc)
{
	std::for_each(m_entities.begin(), m_entities.end(), [=, &gc](__NS_JRE_ECS__::GameObject *rect) {
		if (rect->getParent() == NULL)
			rect->render(gc);
	});
}
__NS_JRE_ENDDEF__
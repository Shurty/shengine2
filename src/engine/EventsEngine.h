/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"

__NS_JRE_DEF__
typedef std::function<BOOL(EventBroadcaster*)> EVFILTERFUNC;

class EventsEngine
{
private:
	BASICVECT<EventBroadcaster*>		m_dispatchers;

public:
	EventsEngine();
	~EventsEngine();

	VOID registerDispatcher(EventBroadcaster*);

	VOID broadcast(EVENT ev, EventData const &evData, EVENT_TYPE type);
	VOID broadcast(EVENT ev, EventData const &evData, EVFILTERFUNC evfilter = NULL);
};
__NS_JRE_ENDDEF__
/////////////////////////////////////////////////////////////////////////////////
//
// This file is part of the "JREngine" source code.
// Use, disclosure, or copying without written consent is strictly prohibited.
//
// Copyright Jeremy DUBUC 2015. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#pragma once

__NS_JRE_DEF__
class AssetsEngine
{
private:
	//TODO: multi-handler system for static/dynamic assets separation
	IAssetsHandler* m_handler;

	// Handle font, textures, shaders
	RenderingEngine* m_render;

public:
	AssetsEngine();
	~AssetsEngine();

	VOID init(RenderingEngine* );
	VOID load();
	VOID unload();

	// accessors
	RenderingEngine* getRenderingEngine() const;

	// g/s
	IAssetsHandler* getHandler();
	VOID setHandler(IAssetsHandler*);
};
__NS_JRE_ENDDEF__
